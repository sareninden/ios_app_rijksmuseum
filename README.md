# README

## Setup

Request a valid token for the api.
Create a `secrets.xcconfig` file in the root of the project like

```
API_TOKEN = your_token
```

You might also need to set a different team if you want to run this app on your own device

## Tests 
for this demo only unit tests are written. Snapshot and/or ui tests where considered out of scope for this project
The test are not complete, they are intended to show the pattern for how to test functionality. For a real life app these test will be more complete and more tooling would be generated to improve testing convenience.

## Improvement ideas
* Perhaps make appearance a singleton due to the nature of cell styling
* Reduce test repetitive code
* Test parsing data in the api framework
* Move framework into their own repositories
* Include the decodable type in the request function in the api framework instead of having the app needing to know this.
* Display more details
* Image loading should be done via a queue instead of all at the same time
* Open images in full screen zoomable view
* Display images in details with the correct aspect ratio 
* Searching for example for `Picasso` causes a parsing error since a invalid `headerImage` is returned. Can be solved by manually parsing and catching this.
* Load error should not clear current content

## Frameworks
* Kingfisher: is a third party library used to retrieve and cache images. The logic is wrapped in a single entry point in case it should be swapped with a custom or other implementation
* ConstraintHelper: a library developed by myself, simply dragged the code in this project
* Appearance: largely copied from an experimental project of my own.
* RijksMuseumApi: Developed entirely for this project

