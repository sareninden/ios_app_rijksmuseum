import UIKit

protocol ParentCoordinator: Coordinator, Childable {

    var childCoordinators: [Coordinator] { get set }

    func start(child: Coordinator) -> UIViewController
    func childCompleted(_ child: Coordinator)
}

protocol Coordinator: AnyObject {

    var parent: Childable? { get set }

    func start() -> UIViewController
}

protocol Childable {
    func childCompleted(_ child: Coordinator)
}

extension ParentCoordinator {

    func start(child: Coordinator) -> UIViewController {

        childCoordinators.append(child)
        child.parent = self
        return child.start()
    }

    func childCompleted(_ child: Coordinator) {

        childCoordinators.removeAll() { child === $0 }
    }
}
