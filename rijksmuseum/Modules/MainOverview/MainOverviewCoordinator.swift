import UIKit
import RijksMuseumApi
import Appearance

enum MainOverviewScene: Equatable {
    case details(ArtObject)
}

final class MainOverviewCoordinator: ParentCoordinator {

    var parent: Childable?

    var childCoordinators: [Coordinator] = []

    private let environment: MainOverviewEnvironment

    init(environment: MainOverviewEnvironment) {
        self.environment = environment
    }

    func start() -> UIViewController {
        let resultsPerPage = UITraitCollection.current.value(
            pad: 25, default: 14
        )
        
        let viewModel = MainOverviewViewModel(
            initialState: .initial(
                resultsPerPage: resultsPerPage,
                language: environment.apiClient.language
            ),
            environment: environment
        )

        let viewController = MainOverviewViewController(
            viewModel: viewModel,
            appearance: environment.appearance
        )

        viewModel.presentScene = { [weak self, weak viewController] scene in
            guard let viewController = viewController else {
                return
            }

            switch scene {
            case let .details(artObject):
                self?.presentDetails(artObject: artObject, context: viewController)
            }
        }

        return viewController
    }

    private func presentDetails(artObject: ArtObject, context: UIViewController) {

        let coordinator = DetailCoordinator(
            artObject: artObject,
            environment: environment.detailEnvironment
        )

        let viewController = start(child: coordinator)

        context.present(viewController, animated: true)
    }
}
