import UIKit
import Appearance
import ConstraintHelper
import SwiftUI

final class MainOverviewViewController: UIViewController {

    private let viewModel: MainOverviewViewModel
    private var previousState: MainOverviewViewModel.State?
    private let appearance: Appearance
    private let flowLayout = UICollectionViewFlowLayout()
    private let collectionView: UICollectionView
    private let emptyView: EmptyView
    private let errorView: ErrorView
    private let activityIndicatorView = UIActivityIndicatorView(style: .medium)
    private let searchBar = UISearchBar()
    private let collectionViewAdapter: MainOverviewCollectionAdapter
    private var languageSegmentedControl = UISegmentedControl() {
        didSet {
            navigationItem.rightBarButtonItem = UIBarButtonItem(
                customView: languageSegmentedControl
            )

            languageSegmentedControl.addTarget(
                self,
                action: #selector(self.changedLanguage(_:)),
                for: .valueChanged
            )
        }
    }

    init(
        viewModel: MainOverviewViewModel,
        appearance: Appearance
    ) {
        self.viewModel = viewModel
        self.appearance = appearance
        self.emptyView = EmptyView(appearance: appearance)
        self.errorView = ErrorView(appearance: appearance)

        self.collectionView = UICollectionView(
            frame: .zero,
            collectionViewLayout: flowLayout
        )

        self.collectionViewAdapter = MainOverviewCollectionAdapter(
            collectionView: collectionView,
            appearance: appearance
        )

        super.init(nibName: nil, bundle: nil)

        title = L10n.MainOverview.title
        configureNavigationItem()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func configureNavigationItem() {
        navigationItem.titleView = searchBar
        searchBar.delegate = self
        searchBar.placeholder = L10n.MainOverview.searchPlaceholder
    }
}

// MARK: Load View
extension MainOverviewViewController {
    override func loadView() {
        view = UIView()
        view.backgroundColor = appearance.color(for: .background(.primary))

        configureViewHierarchy()
        configureFlowLayout()
        configureCollectionView()
        configureEmptyView()
        configureErrorView()
        configureActivityIndicator()

        viewModel.delegate = self
        render(viewModel.state)
    }

    private func configureViewHierarchy() {
        [emptyView, errorView, collectionView, activityIndicatorView]
            .activateAutoLayout()
            .add(to: view)
    }

    private func configureFlowLayout() {
        flowLayout.scrollDirection = .vertical
        flowLayout.minimumLineSpacing = appearance.spacing(for: .s)
        flowLayout.minimumInteritemSpacing = appearance.spacing(for: .s)
    }

    private func configureCollectionView() {
        collectionView.keyboardDismissMode = .onDrag
        collectionView.backgroundColor = appearance.color(for: .background(.primary))
        collectionView.contentInset = [
            .horizontal: appearance.spacing(for: .s),
            .top: appearance.spacing(for: .l),
            .bottom: appearance.spacing(for: .xxxxl)
        ]
        collectionView.register(cell: MainOverviewItemCell.self)
        collectionView.pinEdgesToSuperview()

        collectionViewAdapter.configure(collectionView)
        collectionViewAdapter.selectedItem = { [weak viewModel] in
            viewModel?.perform(.selected($0))
        }
        collectionViewAdapter.loadMore = { [weak viewModel] in
            viewModel?.perform(.loadNextPage)
        }
    }

    private func configureEmptyView() {
        emptyView.pinWidth(400, relation: .lessThanOrEqual)
        emptyView.pinCenterToSuperview(layoutArea: .safeArea)
        emptyView.pinEdgesToSuperview(
            layoutArea: .safeArea,
            relation: .greaterThanOrEqual
        )

        emptyView.render(
            .init(
                title: L10n.MainOverview.emptyTitle,
                message: L10n.MainOverview.emptyMessage,
                actionTitle: L10n.MainOverview.emptyAction
            )
        )

        emptyView.onActionTap = { [viewModel] in
            viewModel.perform(.cleanLoad)
        }
    }

    private func configureErrorView() {
        errorView.pinWidth(400, relation: .lessThanOrEqual)
        errorView.pinCenterToSuperview(layoutArea: .safeArea)
        errorView.pinEdgesToSuperview(
            layoutArea: .safeArea,
            relation: .greaterThanOrEqual
        )

        errorView.render(
            .init(
                title: L10n.MainOverview.errorTitle,
                message: L10n.MainOverview.errorMessage,
                actionTitle: L10n.MainOverview.errorAction
            )
        )

        errorView.onActionTap = { [viewModel] in
            viewModel.perform(.cleanLoad)
        }
    }

    private func configureActivityIndicator() {
        activityIndicatorView.pinCenterToSuperview(layoutArea: .layoutMargins)
    }
}

// MARK: Life cycle
extension MainOverviewViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        viewModel.perform(.cleanLoad)
    }
}

// MARK: Layout
extension MainOverviewViewController {
    override func viewWillTransition(
        to size: CGSize,
        with coordinator: UIViewControllerTransitionCoordinator
    ) {
        super.viewWillTransition(to: size, with: coordinator)

        coordinator.animate(alongsideTransition: { _ in
            self.flowLayout.invalidateLayout()
        }, completion: nil)
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()

        updateFlowLayoutItemSize()
    }

    private func updateFlowLayoutItemSize() {
        let contentWidth = collectionView.bounds.width
        - collectionView.contentInset.width
        let availableItemWidth = contentWidth
        + flowLayout.minimumInteritemSpacing

        let itemSizeForCalculation: CGFloat = traitCollection.value(
            pad: traitCollection.value(regular: 280, default: 220),
            default: 300
        )

        let itemsPerRow = floor(availableItemWidth / itemSizeForCalculation)
        let itemWidth = floor(availableItemWidth / itemsPerRow) - flowLayout.minimumInteritemSpacing

        flowLayout.itemSize = CGSize(
            width: itemWidth,
            height: floor(0.66 * itemWidth)
        )

        flowLayout.headerReferenceSize = CGSize(
            width: contentWidth,
            height: 70
        )
    }
}

// MARK: Data Driven
extension MainOverviewViewController {
    private func render(_ state: MainOverviewViewModel.State) {

        defer {
            previousState = state
        }

        let changedSegmentedControl = state.supportedLanguages != previousState?.supportedLanguages
        if changedSegmentedControl {
            languageSegmentedControl = UISegmentedControl(
                items: state.supportedLanguages.map(\.flagText)
            )
        }

        if changedSegmentedControl || state.language != previousState?.language,
           let index = state.supportedLanguages.firstIndex(of: state.language) {
            languageSegmentedControl.selectedSegmentIndex = index
        }

        guard state.contentState != previousState?.contentState else {
            return
        }

        emptyView.isHidden = state.contentState != .empty
        errorView.isHidden = state.contentState != .error

        if state.contentState == .loading {
            activityIndicatorView.isHidden = false
            activityIndicatorView.startAnimating()
        } else {
            activityIndicatorView.isHidden = true
            activityIndicatorView.stopAnimating()
        }

        if case let .content(content) = state.contentState {
            collectionView.isHidden = false
            collectionViewAdapter.setArtObjects(content.items)
            collectionView.reloadData()
        } else {
            collectionView.isHidden = true
        }
    }
}

// MARK: User Interaction
extension MainOverviewViewController {
    @objc
    private func changedLanguage(_ sender: UISegmentedControl) {
        viewModel.perform(.setLanguage(index: sender.selectedSegmentIndex))
    }
}

// MARK: MainOverviewViewModelDelegate
extension MainOverviewViewController: MainOverviewViewModelDelegate {
    func viewModelUpdatedState(_ viewModel: MainOverviewViewModel) {
        render(viewModel.state)
    }
}

// MARK: UISearchBarDelegate
extension MainOverviewViewController: UISearchBarDelegate {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()

        if let text = searchBar.text, text.isEmpty == false {
            viewModel.perform(.search(text))
        }
    }

    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        if traitCollection.userInterfaceIdiom == .phone {
            navigationItem.setRightBarButtonItems(nil, animated: false)
            searchBar.setShowsCancelButton(true, animated: false)
        }
    }

    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        if searchBar.text == nil || searchBar.text?.isEmpty == true {
            viewModel.perform(.search(nil))
        }

        if traitCollection.userInterfaceIdiom == .phone {
            searchBar.setShowsCancelButton(false, animated: false)

            navigationItem.setRightBarButton(
                UIBarButtonItem(customView: languageSegmentedControl),
                animated: false
            )
        }
    }

    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.text = nil
        searchBar.resignFirstResponder()

        viewModel.perform(.search(nil))
    }
}
