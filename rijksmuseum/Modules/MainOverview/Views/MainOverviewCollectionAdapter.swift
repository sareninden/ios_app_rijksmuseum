import UIKit
import RijksMuseumApi
import Appearance

final class MainOverviewCollectionAdapter: UICollectionViewDiffableDataSource<MainOverviewSection, ArtObject> {
    private let appearance: Appearance

    var selectedItem: ((ArtObject) -> Void)?
    var loadMore: (() -> Void)?

    init(collectionView: UICollectionView, appearance: Appearance) {
        self.appearance = appearance

        super.init(
            collectionView: collectionView
        ) { collectionView, indexPath, artObject in
            collectionView.dequeueReusableCell(
                for: MainOverviewItemCell.self,
                   indexPath: indexPath
            ).render(artObject, appearance:appearance)
        }
    }

    func configure(_ collectionView: UICollectionView) {
        collectionView.register(cell: MainOverviewItemCell.self)
        collectionView.register(
            view: MainOverviewSectionHeader.self,
            for: .header
        )

        collectionView.delegate = self
        collectionView.dataSource = self
    }

    func setArtObjects(_ artObjects: [[ArtObject]]) {
        var snapShot = NSDiffableDataSourceSnapshot<MainOverviewSection, ArtObject>()

        let sections = artObjects.indices.map(MainOverviewSection.init(id:))

        if sections.isEmpty == false {
            snapShot.appendSections(sections)
            sections.forEach { section in
                snapShot.appendItems(artObjects[section.id], toSection: section)
            }
        }

        apply(snapShot)
    }

    func section(atSectionIndex sectionIndex: Int) -> MainOverviewSection {
        snapshot().sectionIdentifiers[sectionIndex]
    }

    func artObject(at indexPath: IndexPath) -> ArtObject {
        snapshot(for: section(atSectionIndex: indexPath.section))
            .items[indexPath.item]
    }

    override func collectionView(
        _ collectionView: UICollectionView,
        viewForSupplementaryElementOfKind kindString: String,
        at indexPath: IndexPath
    ) -> UICollectionReusableView {

        let kind = UICollectionViewSupplementaryViewKind(value: kindString)

        guard kind == .header else {
            fatalError()
        }

        let section = self.section(atSectionIndex: indexPath.section)

        return collectionView.dequeueReusableSupplementaryView(
            for: MainOverviewSectionHeader.self,
               kind: kind,
               indexPath: indexPath
        ).render(section.sectionTitle, appearance: appearance)
    }
}

// MARK: - UICollectionViewDelegate
extension MainOverviewCollectionAdapter: UICollectionViewDelegate {
    func collectionView(
        _ collectionView: UICollectionView,
        didSelectItemAt indexPath: IndexPath
    ) {
        collectionView.deselectItem(at: indexPath, animated: true)

        selectedItem?(artObject(at: indexPath))
    }

    func collectionView(
        _ collectionView: UICollectionView,
        didHighlightItemAt indexPath: IndexPath
    ) {
        guard let cell = collectionView.cellForItem(at: indexPath) else {
            return
        }

        UIView.animate(withDuration: 0.25) {
            cell.transform = CGAffineTransform(scaleX: 0.95, y: 0.95)
        }
    }

    func collectionView(
        _ collectionView: UICollectionView,
        didUnhighlightItemAt indexPath: IndexPath
    ) {
        guard let cell = collectionView.cellForItem(at: indexPath) else {
            return
        }

        UIView.animate(withDuration: 0.25) {
            cell.transform = .identity
            cell.alpha = 1
        }
    }
}

// MARK: - UIScrollViewDelegate
extension MainOverviewCollectionAdapter: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {

        guard let loadMore = loadMore else {
            return
        }

        let bottomOffset = scrollView.contentOffset.y + scrollView.bounds.height
        let threshold = scrollView.contentSize.height - (0.5 * scrollView.bounds.height)

        if bottomOffset > threshold {
            loadMore()
        }
    }
}
