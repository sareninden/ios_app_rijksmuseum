import UIKit
import Appearance
import RijksMuseumApi
import ConstraintHelper

final class MainOverviewItemCell: UICollectionViewCell {
    private let activityIndicatorView = UIActivityIndicatorView(style: .medium)
    private let titleLabel = UILabel()
    private let imageView = UIImageView()
    private let blurEffect = UIBlurEffect(style: .regular)
    private let titleBackground: UIVisualEffectView
    private let vibrancyTitleView: UIVisualEffectView

    override init(frame: CGRect) {
        self.titleBackground = UIVisualEffectView(effect: self.blurEffect)
        self.vibrancyTitleView = UIVisualEffectView(
            effect: UIVibrancyEffect(blurEffect: self.blurEffect)
        )

        super.init(frame: frame)

        configureView()
        traitCollectionDidChange(nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func prepareForReuse() {
        super.prepareForReuse()

        imageView.cancelImageLoading()
        imageView.image = nil

        titleLabel.text = nil
        setLoading(false)
        transform = .identity
    }

    override func traitCollectionDidChange(
        _ previousTraitCollection: UITraitCollection?
    ) {
        super.traitCollectionDidChange(previousTraitCollection)

        contentView.layer.rasterizationScale = traitCollection.displayScale
    }
}

// MARK: - Configure views
extension MainOverviewItemCell {
    private func configureView() {
        createViewHierarchy()
        configureLayer()
        configureContentView()
        configureBackgroundView()
        configureImageView()
        configureTitleLabel()
        configureTitleBackground()
        configureVibrancyTitleView()
        configureActivityIndicatorView()
    }

    private func createViewHierarchy() {
        [imageView, titleBackground, activityIndicatorView]
            .activateAutoLayout()
            .add(to: contentView)

        titleBackground.contentView.addSubview(vibrancyTitleView.activateAutoLayout())
        vibrancyTitleView.contentView.addSubview(titleLabel.activateAutoLayout())
    }

    private func configureLayer() {
        layer.shadowOffset = CGSize(width: 2, height: 2)
        layer.shadowOpacity = 0.3
        layer.masksToBounds = false
    }

    private func configureContentView() {
        contentView.layer.masksToBounds = true
        contentView.layer.shouldRasterize = true
    }

    private func configureBackgroundView() {
        backgroundView = UIView(frame: bounds)
        backgroundView?.layer.masksToBounds = true

        selectedBackgroundView = UIView(frame: bounds)
        selectedBackgroundView?.layer.masksToBounds = true
    }

    private func configureImageView() {
        imageView.isUserInteractionEnabled = false
        imageView.pinEdgesToSuperview()
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
    }

    private func configureTitleLabel() {
        titleLabel.isUserInteractionEnabled = false
        titleLabel.pinCenter(to: titleBackground)
        titleLabel.pinEdgesToSuperview(
            layoutArea: .layoutMargins,
            relation: .greaterThanOrEqual
        )
    }

    private func configureTitleBackground() {
        titleBackground.isUserInteractionEnabled = false
        titleBackground.pinEdgesToSuperview(excludeEdges: .top)
        titleBackground.pinHeight(40)
    }

    private func configureVibrancyTitleView() {
        vibrancyTitleView.pinEdgesToSuperview()
    }

    private func configureActivityIndicatorView() {
        activityIndicatorView.pinCenterToSuperview()
    }
}

// MARK: - Layout subviews
extension MainOverviewItemCell {
    override func layoutSubviews() {
        super.layoutSubviews()

        layer.shadowPath = UIBezierPath(rect: bounds).cgPath
    }
}

// MARK: - Data Driven
extension MainOverviewItemCell {
    @discardableResult
    func render(
        _ artObject: ArtObject,
        appearance: Appearance
    ) -> Self {
        titleLabel.text = artObject.title
        titleLabel.textColor = appearance.color(for: .foreground(.primary))
        titleLabel.font = appearance.font(for: .title(.title3))

        layer.shadowRadius = appearance.shadowRadius(for: .light)
        layer.shadowColor = appearance.shadowColor(for: .light).cgColor

        contentView.layer.cornerRadius = appearance.cornerRadius(for: .standard)

        backgroundView?.backgroundColor = appearance.color(for: .background(.primary))
        backgroundView?.layer.cornerRadius = appearance.cornerRadius(for: .standard)
        selectedBackgroundView?.backgroundColor = appearance.color(for: .background(.selectedPrimary))
        selectedBackgroundView?.layer.cornerRadius = appearance.cornerRadius(for: .standard)

        if
            let webImage = artObject.webImage ?? artObject.headerImage,
            let url = URL(string: webImage.url) {
            setLoading(true)

            if imageView.bounds.width == 0 {
                layoutIfNeeded()
            }

            imageView.load(url, imageSize: bounds.size) { [weak self] _ in
                self?.setLoading(false)
            }
        } else {
            imageView.image = UIImage(systemName: "photo.on.rectangle")
        }

        return self
    }

    private func setLoading(_ isLoading: Bool) {
        activityIndicatorView.isHidden = isLoading == false

        if isLoading {
            activityIndicatorView.startAnimating()
        } else {
            activityIndicatorView.stopAnimating()
        }
    }
}
