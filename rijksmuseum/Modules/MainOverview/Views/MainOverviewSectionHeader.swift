import UIKit
import Appearance

final class MainOverviewSectionHeader: UICollectionReusableView {
    private let titleLabel = UILabel()

    override init(frame: CGRect) {
        super.init(frame: frame)

        configureView()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func prepareForReuse() {
        super.prepareForReuse()

        titleLabel.text = nil
    }
}

// MARK: - Configure View
extension MainOverviewSectionHeader {
    private func configureView() {
        createViewHierarchy()
        configureSelf()
        configureTitleLabel()
    }

    private func createViewHierarchy() {
        [titleLabel]
            .activateAutoLayout()
            .add(to: self)
    }

    private func configureSelf() {
        preservesSuperviewLayoutMargins = false
        insetsLayoutMarginsFromSafeArea = false
    }

    private func configureTitleLabel() {
        titleLabel.pinEdgesToSuperview(
            layoutArea: .layoutMargins,
            excludeEdges: [.top, .trailing]
        )
        titleLabel.pinTrailingToSuperview(
            layoutArea: .layoutMargins,
            relation: .greaterThanOrEqual
        )

        setContentHuggingPriority(.required, for: .vertical)
        setContentCompressionResistancePriority(.required, for: .vertical)

        titleLabel.setContentHuggingPriority(.required, for: .vertical)
        titleLabel.setContentCompressionResistancePriority(.required, for: .vertical)
    }
}

// MARK: - Data Driven
extension MainOverviewSectionHeader {
    @discardableResult
    func render(_ title: String, appearance: Appearance) -> Self {
        titleLabel.text = title
        titleLabel.font = appearance.font(for: .title(.title2))
        titleLabel.textColor = appearance.color(for: .foreground(.primary))

        directionalLayoutMargins = appearance.edgeInsets(
            horizontal: .m,
            bottom: .s
        )

        return self
    }
}

