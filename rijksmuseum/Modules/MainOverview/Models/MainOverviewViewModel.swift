import Foundation
import CloudKit
import RijksMuseumApi
import SwiftUI

protocol MainOverviewViewModelDelegate: AnyObject {

    func viewModelUpdatedState(_ viewModel: MainOverviewViewModel)
}

extension MainOverviewViewModel {

    struct Content: Equatable {
        let items: [[ArtObject]]
        let count: Int
    }

    enum ContentState: Equatable {
        case empty
        case error
        case loading
        case content(Content)

        var artObjects: [[ArtObject]] {
            guard case let .content(content) = self else {
                return []
            }

            return content.items
        }
    }

    struct State: Equatable {
        let resultsPerPage: Int
        var language: ApiLanguage
        let supportedLanguages: [ApiLanguage]
        var isLoadingMore = false
        var nextPage = 1
        var canLoadMore = true
        var contentState: MainOverviewViewModel.ContentState
        var query: String?

        static func initial(
            resultsPerPage: Int,
            language: ApiLanguage,
            supportedLanguages: [ApiLanguage] = ApiLanguage.allCases
        ) -> Self {
            Self(
                resultsPerPage: resultsPerPage,
                language: language,
                supportedLanguages: supportedLanguages,
                contentState: .loading
            )
        }
    }
}

extension MainOverviewViewModel {
    enum Action: Equatable {
        case setLanguage(index: Int)
        case cleanLoad
        case loadNextPage
        case selected(ArtObject)
        case search(String?)
    }
}

final class MainOverviewViewModel {
    weak var delegate: MainOverviewViewModelDelegate?
    var presentScene: ((MainOverviewScene) -> Void)?

    private(set) var state: State
    private let environment: MainOverviewEnvironment

    init(initialState: State, environment: MainOverviewEnvironment) {
        self.state = initialState
        self.environment = environment
    }

    func perform(_ action: Action) {
        switch action {
        case .cleanLoad:
            performCleanLoad()
        case .loadNextPage:
            loadNextPageIfAllowed()
        case let .selected(artObject):
            presentScene?(.details(artObject))
        case let .setLanguage(index):
            setLanguage(state.supportedLanguages[index])
            performCleanLoad()
        case let .search(query):
            guard state.query != query else {
                return
            }

            state.query = query
            performCleanLoad()
        }
    }

    private func performCleanLoad() {
        state.contentState = .loading
        state.nextPage = 1
        state.canLoadMore = true
        state.isLoadingMore = false

        loadPage(state.nextPage)
    }

    private func loadNextPageIfAllowed() {
        guard case let .content(content) = state.contentState else {
            return
        }

        let canLoadMore = content.count > state.nextPage * state.resultsPerPage

        guard canLoadMore, state.isLoadingMore == false else {
            return
        }

        loadPage(state.nextPage)
    }

    private func loadPage(_ pageOffset: Int) {
        defer { updatedState() }

        state.isLoadingMore = pageOffset != 0

        try? environment.apiClient.execute(
            resultType: CollectionResponse.self,
            request: APIURLRequest.collections(
                query: state.query,
                pageOffset: pageOffset,
                resultsPerPage: state.resultsPerPage
            )
        ) { [weak self] result in
            defer {
                self?.state.isLoadingMore = false
                self?.updatedState()
            }

            switch result {
            case let .success(content):

                self?.received(content)

                self?.state.nextPage = pageOffset + 1

            case .failure:
                self?.state.contentState = .error
                self?.state.nextPage = 1
            }
        }
    }

    private func received(_ response: CollectionResponse) {

        guard response.artObjects.isEmpty == false else {
            return
        }

        state.contentState = .content(
            Content(
                items: state.contentState.artObjects + [response.artObjects],
                count: response.count
            )
        )
    }

    private func setLanguage(_ language: ApiLanguage) {
        state.language = language
        environment.apiClient.language = language
        environment.storage.set(
            language.rawValue,
            forKey: StorageKeys.language.key
        )
    }

    private func updatedState() {

        DispatchQueue.main.async {
            self.delegate?.viewModelUpdatedState(self)
        }
    }
}
