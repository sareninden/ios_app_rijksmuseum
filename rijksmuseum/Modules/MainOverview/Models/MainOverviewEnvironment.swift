import Appearance
import RijksMuseumApi

struct MainOverviewEnvironment {
    let apiClient: APIClient
    let appearance: Appearance
    let storage: Storage
}

extension AppEnvironment {
    var mainOverviewEnvironment: MainOverviewEnvironment {
        MainOverviewEnvironment(
            apiClient: apiClient,
            appearance: appearance,
            storage: storage
        )
    }
}
