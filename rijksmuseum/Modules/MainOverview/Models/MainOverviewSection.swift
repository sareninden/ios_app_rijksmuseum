import RijksMuseumApi

struct MainOverviewSection: Identifiable, Hashable {
    let id: Int

    var sectionTitle: String {
        L10n.MainOverview.sectionHeader(forPage: id + 1)
    }
}
