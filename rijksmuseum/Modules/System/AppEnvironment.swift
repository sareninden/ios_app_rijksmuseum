import Appearance
import RijksMuseumApi

struct AppEnvironment {
    let apiClient: APIClient
    let appearance: Appearance
    let storage: Storage
}
