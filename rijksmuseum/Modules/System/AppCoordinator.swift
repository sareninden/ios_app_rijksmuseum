import UIKit

final class AppCoordinator: ParentCoordinator {

    var parent: Childable?
    var childCoordinators: [Coordinator] = []

    private let mainOverviewCoordinator: MainOverviewCoordinator

    init(mainOverviewCoordinator: MainOverviewCoordinator) {
        self.mainOverviewCoordinator = mainOverviewCoordinator
    }

    func start() -> UIViewController {
        let navigationController = UINavigationController(
            rootViewController: start(child: mainOverviewCoordinator)
        )

        let appearance = UINavigationBarAppearance()
        appearance.configureWithDefaultBackground()

        navigationController.navigationBar.compactAppearance = appearance
        navigationController.navigationBar.standardAppearance = appearance
        navigationController.navigationBar.scrollEdgeAppearance = appearance

        return navigationController
    }

    func childCompleted(_ child: Coordinator) {
        assertionFailure("Unexpected completion")
    }
}
