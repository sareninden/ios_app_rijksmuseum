import UIKit
import Appearance
import RijksMuseumApi

final class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?

    private var rootViewController: UIViewController?

    func scene(
        _ scene: UIScene,
        willConnectTo session: UISceneSession,
        options connectionOptions: UIScene.ConnectionOptions
    ) {
        guard let windowScene = (scene as? UIWindowScene) else {
            return
        }

        let environment = createAppEnvironment()
        let coordinator = AppCoordinator(
            mainOverviewCoordinator: MainOverviewCoordinator(
                environment: environment.mainOverviewEnvironment
            )
        )

        let window = UIWindow(windowScene: windowScene)

        window.tintColor = environment.appearance.color(for: .tint(.primary))
        window.rootViewController = coordinator.start()
        window.makeKeyAndVisible()

        self.window = window
    }

    private func createAppEnvironment() -> AppEnvironment {

        let storage: Storage = UserDefaults.standard

        return AppEnvironment(
            apiClient: createApiClient(storage: storage),
            appearance: AppearanceConcrete(),
            storage: storage
        )
    }
    private func createApiClient(storage: Storage) -> APIClient {

        guard
            let apiUrlString = Bundle.main.object(forInfoDictionaryKey: "API_URL") as? String,
            let baseURL = URL(string: apiUrlString),
            let apiToken = Bundle.main.object(forInfoDictionaryKey: "API_TOKEN") as? String else {
                fatalError()
            }

        let client = APIClientURLSession(
            baseURL: baseURL,
            apiToken: apiToken
        )

        if let value = storage.string(forKey: StorageKeys.language.key) {
            client.language = ApiLanguage(rawValue: value) ?? .en
        }

        return client
    }
}
