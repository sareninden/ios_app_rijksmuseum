import UIKit
import Appearance
import ConstraintHelper

extension ErrorView {
    struct State: Equatable {
        let title: String
        let message: String
        let actionTitle: String
    }
}

final class ErrorView: UIView {

    private let appearance: Appearance
    private let contentStackView = UIStackView()
    private let titleLabel = UILabel()
    private let messageLabel = UILabel()
    private let actionButton = UIButton()

    var onActionTap: (() -> Void)?

    init(appearance: Appearance) {
        self.appearance = appearance

        super.init(frame: .zero)

        configureView()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

// MARK: - Configure View
extension ErrorView {
    private func configureView() {
        createViewHierarchy()
        configureContentStackView()
        configureTitleLabel()
        configureMessageLabel()
        configureActionButton()
    }

    private func createViewHierarchy() {
        [contentStackView]
            .activateAutoLayout()
            .add(to: self)

        [titleLabel, messageLabel, actionButton]
            .forEach(contentStackView.addArrangedSubview)
    }

    private func configureContentStackView() {
        contentStackView.pinEdgesToSuperview()

        contentStackView.axis = .vertical
        contentStackView.spacing = appearance.spacing(for: .m)
        contentStackView.distribution = .fill
        contentStackView.alignment = .center
        contentStackView.preservesSuperviewLayoutMargins = false
        contentStackView.isLayoutMarginsRelativeArrangement = true
        contentStackView.directionalLayoutMargins = NSDirectionalEdgeInsets(
            horizontal: appearance.spacing(for: .l),
            vertical: appearance.spacing(for: .m)
        )
    }

    private func configureTitleLabel() {
        titleLabel.textColor = appearance.color(for: .foreground(.primary))
        titleLabel.font = appearance.font(for: .title(.title1))
        titleLabel.numberOfLines = 0
        titleLabel.textAlignment = .center
    }

    private func configureMessageLabel() {
        messageLabel.textColor = appearance.color(for: .foreground(.secondary))
        messageLabel.font = appearance.font(for: .body(.regular))
        messageLabel.numberOfLines = 0
        messageLabel.textAlignment = .center
    }

    private func configureActionButton() {

        actionButton.setTitleColor(
            appearance.color(for: .foreground(.tint)),
            for: .normal
        )

        actionButton.addTarget(
            self,
            action: #selector(self.tappedActionButton),
            for: .touchUpInside
        )
    }
}

// MARK: - Data Driven
extension ErrorView {
    func render(_ state: State) {
        titleLabel.text = state.title
        messageLabel.text = state.message
        actionButton.setTitle(state.actionTitle, for: .normal)
    }
}

// MARK: - User Interaction
extension ErrorView {

    @objc
    private func tappedActionButton(_ sender: UIButton) {
        onActionTap?()
    }
}
