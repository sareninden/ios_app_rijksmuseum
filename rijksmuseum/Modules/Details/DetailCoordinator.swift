import UIKit
import RijksMuseumApi
import Appearance

enum DetailCoordinatorAction: Equatable {
    case closed
}

final class DetailCoordinator: Coordinator {
    var parent: Childable?

    private let artObject: ArtObject
    private let environment: DetailEnvironment

    init(
        artObject: ArtObject,
        environment: DetailEnvironment
    ) {
        self.artObject = artObject
        self.environment = environment
    }

    func start() -> UIViewController {
        let viewModel = DetailViewModel(
            initialState: .initial(artObject: artObject),
            environment: environment
        )

        viewModel.coordinatorAction = { [weak self] in
            guard let self = self else {
                return
            }

            switch $0 {
            case .closed:
                self.parent?.childCompleted(self)
            }
        }

        let viewController = DetailViewController(
            viewModel: viewModel,
            appearance: environment.appearance
        )

        return viewController
    }
}
