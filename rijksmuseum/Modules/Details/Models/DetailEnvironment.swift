import RijksMuseumApi
import Appearance

struct DetailEnvironment {
    let apiClient: APIClient
    let appearance: Appearance
}

extension MainOverviewEnvironment {
    var detailEnvironment: DetailEnvironment {
        DetailEnvironment(
            apiClient: apiClient,
            appearance: appearance
        )
    }
}
