struct DetailSection: Equatable {
    let sectionType: SectionType
    let sectionTitle: String?
    let items: [DetailItem]

    var count: Int { items.count }

    init(
        sectionType: SectionType,
        sectionTitle: String? = nil,
        items: [DetailItem]
    ) {
        self.sectionType = sectionType
        self.sectionTitle = sectionTitle
        self.items = items
    }

    func item(atIndex index: Int) -> DetailItem {
        items[index]
    }
}

extension DetailSection {

    enum SectionType: Equatable {
        case headerImage
        case title
        case titleAndDescription
        case materials
        case techniques
        case dimensions
    }
}
