import UIKit
import ConstraintHelper
import Appearance

private final class PillBackground: UIView {

    override func layoutSubviews() {
        super.layoutSubviews()

        layer.cornerRadius = 0.5 * bounds.height
    }
}

final class DetailPillCell: UICollectionViewCell {
    private let titleBackgroundView = PillBackground()
    private let titleLabel = UILabel()

    override init(frame: CGRect) {
        super.init(frame: frame)

        configureView()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func prepareForReuse() {
        super.prepareForReuse()

        titleLabel.text = nil
    }
}

// MARK: - Configure View
extension DetailPillCell {
    private func configureView() {
        createViewHierarchy()
        configureContentView()
        configureTitleBackgroundView()
        configureTitleLabel()
    }

    private func createViewHierarchy() {
        contentView.addSubview(titleBackgroundView.activateAutoLayout())
        titleBackgroundView.addSubview(titleLabel.activateAutoLayout())
    }

    private func configureContentView() {
        contentView.preservesSuperviewLayoutMargins = false
        contentView.insetsLayoutMarginsFromSafeArea = false
    }

    private func configureTitleBackgroundView() {
        titleBackgroundView.pinEdgesToSuperview()
        titleBackgroundView.layer.masksToBounds = true
    }

    private func configureTitleLabel() {
        titleLabel.pinEdgesToSuperview(layoutArea: .layoutMargins)

        setContentHuggingPriority(.required, for: .vertical)
        setContentCompressionResistancePriority(.required, for: .vertical)

        titleLabel.numberOfLines = 0
        titleLabel.setContentHuggingPriority(.required, for: .vertical)
        titleLabel.setContentCompressionResistancePriority(.required, for: .vertical)
    }
}

// MARK: - Data Driven
extension DetailPillCell {
    @discardableResult
    func render(_ title: String, appearance: Appearance) -> Self {
        titleLabel.text = title
        titleLabel.font = appearance.font(for: .body(.regularMedium))
        titleLabel.textColor = appearance.color(for: .foreground(.primary))

        titleBackgroundView.backgroundColor = appearance.color(
            for: .background(.pill)
        )

        titleBackgroundView.directionalLayoutMargins = appearance.edgeInsets(
            horizontal: .m,
            vertical: .xs
        )

        return self
    }
}
