import Foundation

enum DetailItem: Equatable {
    case headerImage(URL)
    case title(String)
    case titleDescription(DetailTitleDescriptionCell.State)
    case material(String)
    case technique(String)
    case dimension(String)
}
