import Foundation
import RijksMuseumApi

protocol DetailViewModelModelDelegate: AnyObject {

    func viewModelUpdatedState(_ viewModel: DetailViewModel)
}

extension DetailViewModel {

    struct Content: Equatable {
        let artObjectDetails: ArtObjectDetails
    }

    enum ContentState: Equatable {
        case error
        case loading
        case content(Content)
    }

    struct State: Equatable {
        let artObject: ArtObject
        var contentState: ContentState

        var artObjectDetails: ArtObjectDetails? {
            if case let .content(content) = contentState {
                return content.artObjectDetails
            } else {
                return nil
            }
        }

        static func initial(artObject: ArtObject) -> Self {
            State(
                artObject: artObject,
                contentState: .loading
            )
        }
    }
}

extension DetailViewModel {
    enum Action: Equatable {
        case loadDetails
        case dismissed
    }
}

final class DetailViewModel {
    weak var delegate: DetailViewModelModelDelegate?
    private(set) var state: State
    private let environment: DetailEnvironment

    var coordinatorAction: ((DetailCoordinatorAction) -> Void)?

    init(initialState: State, environment: DetailEnvironment) {
        self.state = initialState
        self.environment = environment
    }

    func perform(_ action: Action) {
        switch action {
        case .loadDetails:
            loadDetails()
        case .dismissed:
            coordinatorAction?(.closed)
        }
    }

    private func loadDetails() {

        defer { updatedState() }
        state.contentState = .loading

        try? environment.apiClient.execute(
            resultType: ArtObjectDetailsResponse.self,
            request: APIURLRequest.artObjectDetails(
                objectNumber: state.artObject.objectNumber
            )
        ) { [weak self] response in
            switch response {
            case let .success(content):
                self?.received(content.artObject)
            case .failure:
                self?.state.contentState = .error
                self?.updatedState()
            }
        }
    }

    private func received(_ artObject: ArtObjectDetails) {

        state.contentState = .content(
            .init(artObjectDetails: artObject)
        )

        updatedState()
    }

    private func updatedState() {

        DispatchQueue.main.async {
            self.delegate?.viewModelUpdatedState(self)
        }
    }
}
