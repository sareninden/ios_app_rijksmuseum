import UIKit

final class DetailFlowLayout: UICollectionViewFlowLayout {

    override init() {
        super.init()

        itemSize = UICollectionViewFlowLayout.automaticSize
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private var delegate: UICollectionViewDelegateFlowLayout? {
        return collectionView?.delegate as? UICollectionViewDelegateFlowLayout
    }

    private func sectionInset(forSectionIndex sectionIndex: Int) -> UIEdgeInsets {
        guard let collectionView = collectionView else {
            return sectionInset
        }

        return delegate?.collectionView?(
            collectionView,
            layout: self,
            insetForSectionAt: sectionIndex
        ) ?? sectionInset
    }

    private func minimumLineSpacing(forSectionIndex sectionIndex: Int) -> CGFloat {
        guard let collectionView = collectionView else {
            return self.minimumLineSpacing
        }

        return delegate?.collectionView?(
            collectionView,
            layout: self,
            minimumLineSpacingForSectionAt: sectionIndex
        ) ?? minimumLineSpacing
    }

    override func layoutAttributesForElements(
        in rect: CGRect
    ) -> [UICollectionViewLayoutAttributes]? {
        guard let attributes = super.layoutAttributesForElements(in: rect) else {
            return nil
        }

        var leftMargin = sectionInset.left
        var maxY: CGFloat = -1.0

        attributes.forEach { layoutAttribute in
            guard layoutAttribute.representedElementCategory == .cell else {
                return
            }

            guard layoutAttribute.frame.minX > 0 else {
                return
            }

            let sectionInset = self.sectionInset(
                forSectionIndex: layoutAttribute.indexPath.section
            )

            let minimumLineSpacing = self.minimumLineSpacing(
                forSectionIndex: layoutAttribute.indexPath.section
            )

            if layoutAttribute.frame.origin.y >= maxY {
                leftMargin = sectionInset.left
            }

            layoutAttribute.frame.origin.x = leftMargin
            leftMargin += layoutAttribute.frame.width + minimumLineSpacing
            maxY = max(layoutAttribute.frame.maxY, maxY)
        }

        return attributes
    }
}
