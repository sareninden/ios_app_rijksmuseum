import UIKit
import Appearance

final class DetailViewController: UIViewController {

    private let viewModel: DetailViewModel
    private let appearance: Appearance

    private let closeButton: DetailCloseButton
    private let collectionView: UICollectionView
    private let activityIndicatorView = UIActivityIndicatorView(style: .medium)
    private let collectionViewAdapter: DetailCollectionAdapter
    private let flowLayout = DetailFlowLayout()

    init(
        viewModel: DetailViewModel,
        appearance: Appearance
    ) {
        self.viewModel = viewModel
        self.appearance = appearance

        self.closeButton = DetailCloseButton(appearance: appearance)
        self.collectionViewAdapter = DetailCollectionAdapter(
            appearance: appearance
        )

        self.collectionView = UICollectionView(
            frame: .zero,
            collectionViewLayout: flowLayout
        )

        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()

        flowLayout.headerReferenceSize = CGSize(
            width: collectionView.bounds.width,
            height: 70
        )

        flowLayout.estimatedItemSize = CGSize(
            width: collectionView.bounds.width,
            height: 50
        )
    }
}

// MARK: Load View
extension DetailViewController {

    override func loadView() {
        view = UIView()
        view.backgroundColor = appearance.color(for: .background(.primary))

        configureViewHierarchy()
        configureCollectionView()
        configureActivityIndicator()
        configureCloseButton()

        viewModel.delegate = self
        render(viewModel.state)
    }

    private func configureViewHierarchy() {
        [collectionView, activityIndicatorView, closeButton]
            .activateAutoLayout()
            .add(to: view)
    }

    private func configureCollectionView() {
        collectionView.pinEdgesToSuperview()
        collectionView.backgroundColor = appearance.color(for: .background(.primary))
        collectionView.contentInset.bottom = appearance.spacing(
            for: traitCollection.value(pad: .xl, default: .m)
        )

        collectionViewAdapter.configure(collectionView)
    }

    private func configureActivityIndicator() {
        activityIndicatorView.pinCenterToSuperview(layoutArea: .layoutMargins)
    }

    private func configureCloseButton() {
        closeButton.pinEdgesToSuperview(
            padding: [.default: appearance.spacing(for: .s)],
            excludeEdges: [.leading, .bottom]
        )

        closeButton.addTarget(
            self,
            action: #selector(self.tappedClose(_:)),
            for: .touchUpInside
        )
    }
}

// MARK: - Life cycle
extension DetailViewController {
    override func viewDidLoad() {
        super.viewDidLoad()

        viewModel.perform(.loadDetails)
    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)

        if isBeingDismissed {
            viewModel.perform(.dismissed)
        }
    }
}

// MARK: Data Driven
extension DetailViewController {
    private func render(_ state: DetailViewModel.State) {
        collectionViewAdapter.setState(state)
        collectionView.reloadData()
    }
}

// MARK: User Interaction
extension DetailViewController {
    @objc
    private func tappedClose(_ sender: UIButton) {
        dismiss(animated: true)
    }
}

// MARK: - DetailViewModelModelDelegate
extension DetailViewController: DetailViewModelModelDelegate {
    func viewModelUpdatedState(_ viewModel: DetailViewModel) {
        render(viewModel.state)
    }
}
