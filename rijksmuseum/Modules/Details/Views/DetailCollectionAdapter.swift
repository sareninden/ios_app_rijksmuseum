import UIKit
import SwiftUI
import RijksMuseumApi
import Appearance

final class DetailCollectionAdapter: NSObject {

    private let appearance: Appearance
    private var sections: [DetailSection] = []

    init(appearance: Appearance) {
        self.appearance = appearance
    }

    func configure(_ collectionView: UICollectionView) {
        collectionView.register(cell: DetailHeaderImageCell.self)
        collectionView.register(cell: DetailTitleCell.self)
        collectionView.register(cell: DetailTitleDescriptionCell.self)
        collectionView.register(cell: DetailPillCell.self)
        collectionView.register(
            view: DetailSectionHeaderView.self,
            for: .header
        )

        collectionView.delegate = self
        collectionView.dataSource = self
    }
}

// MARK: - Create Content
extension DetailCollectionAdapter {
    func setState(_ state: DetailViewModel.State) {
        sections = [
            headerImageSection(for: state),
            titleSection(for: state),
            detailTextsSection(for: state),
            materialsSection(for: state),
            techniquesSection(for: state),
            dimensionsSection(for: state)
        ].compactMap { $0 }
    }

    private func headerImageSection(for state: DetailViewModel.State) -> DetailSection? {
        let images = [
            largestImage(
                [
                    state.artObjectDetails?.webImage,
                    state.artObject.webImage
                ].compactMap({ $0 })
            ),
            largestImage(
                [
                    state.artObjectDetails?.headerImage,
                    state.artObject.headerImage
                ].compactMap({ $0 })
            )
        ]
            .compactMap { $0?.url }
            .compactMap(URL.init)
            .map(DetailItem.headerImage)

        guard images.isEmpty == false else {
            return nil
        }

        return DetailSection(
            sectionType: .headerImage,
            items: [images[0]]
        )
    }

    private func largestImage(_ images: [ImageResponse]) -> ImageResponse? {
        guard images.isEmpty == false else {
            return nil
        }

        let result = images.reduce(images[0]) { current, next in
            return current.height > next.height ? current : next
        }

        return result
    }

    private func titleSection(
        for state: DetailViewModel.State
    ) -> DetailSection {
        DetailSection(
            sectionType: .title,
            items: [
                .title(state.artObjectDetails?.title ?? state.artObject.title)
            ]
        )
    }

    private func materialsSection(
        for state: DetailViewModel.State
    ) -> DetailSection? {
        guard
            let materials = state.artObjectDetails?.materials,
            materials.isEmpty == false
        else {
            return nil
        }

        return DetailSection(
            sectionType: .materials,
            sectionTitle: L10n.Details.sectionTitleMaterials,
            items: materials
                .map { DetailItem.material($0.localizedCapitalized) }
        )
    }

    private func techniquesSection(
        for state: DetailViewModel.State
    ) -> DetailSection? {
        guard
            let techniques = state.artObjectDetails?.techniques,
            techniques.isEmpty == false
        else {
            return nil
        }

        return DetailSection(
            sectionType: .techniques,
            sectionTitle: L10n.Details.sectionTitleTechniques,
            items: techniques
                .map { DetailItem.technique($0.localizedUppercase) }
        )
    }

    private func dimensionsSection(
        for state: DetailViewModel.State
    ) -> DetailSection? {
        guard
            let dimensions = state.artObjectDetails?.dimensions,
            dimensions.isEmpty == false
        else {
            return nil
        }

        let items = dimensions.map {
            DetailItem.dimension("\($0.type): \($0.value) \($0.unit)")
        }

        return DetailSection(
            sectionType: .dimensions,
            sectionTitle: L10n.Details.sectionTitleDimensions,
            items: items
        )
    }

    private func detailTextsSection(
        for state: DetailViewModel.State
    ) -> DetailSection? {
        guard let details = state.artObjectDetails else {
            return nil
        }

        var items: [DetailTitleDescriptionCell.State] = []

        items.append(
            .init(
                title: L10n.Details.itemLongTitle,
                description: details.longTitle
            )
        )

        details.titles.forEach {
            items.append(
                .init(
                    title: L10n.Details.itemOtherTitle,
                    description: $0
                )
            )
        }

        if let description = details.description {
            items.append(
                .init(
                    title: L10n.Details.itemDescriptionTitle,
                    description: description
                )
            )
        }

        return DetailSection(
            sectionType: .headerImage,
            items: items.map(DetailItem.titleDescription)
        )
    }

    func section(atSectionIndex index: Int) -> DetailSection {
        sections[index]
    }

    func item(at indexPath: IndexPath) -> DetailItem {
        section(atSectionIndex: indexPath.section)
            .item(atIndex: indexPath.item)
    }
}

// MARK: - UICollectionViewDelegate
extension DetailCollectionAdapter: UICollectionViewDelegate {

}

// MARK: - UICollectionViewDelegateFlowLayout
extension DetailCollectionAdapter: UICollectionViewDelegateFlowLayout {
    func collectionView(
        _ collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        referenceSizeForHeaderInSection sectionIndex: Int
    ) -> CGSize {
        let section = section(atSectionIndex: sectionIndex)

        guard section.sectionTitle != nil else {
            return .zero
        }

        let sectionInset = self.collectionView(
            collectionView,
            layout: collectionViewLayout,
            insetForSectionAt: sectionIndex
        )

        return CGSize(
            width: collectionView.bounds.width - sectionInset.width,
            height: 70
        )
    }

    func collectionView(
        _ collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        insetForSectionAt sectionIndex: Int
    ) -> UIEdgeInsets {

        let section = self.section(atSectionIndex: sectionIndex)

        switch section.sectionType {
        case .materials, .techniques, .dimensions:
            return UIEdgeInsets(horizontal: appearance.spacing(for: .m))

        case .title:
            return [
                .top: appearance.spacing(for: .xs),
                .bottom: appearance.spacing(for: .s)
            ]
        case .headerImage, .titleAndDescription:
            return .zero
        }
    }
}

// MARK: - UICollectionViewDataSource
extension DetailCollectionAdapter: UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return sections.count
    }

    func collectionView(
        _ collectionView: UICollectionView,
        numberOfItemsInSection index: Int
    ) -> Int {
        section(atSectionIndex: index).count
    }

    func collectionView(
        _ collectionView: UICollectionView,
        cellForItemAt indexPath: IndexPath
    ) -> UICollectionViewCell {
        switch item(at: indexPath) {
        case let .headerImage(url):
            return collectionView.dequeueReusableCell(
                for: DetailHeaderImageCell.self,
                   indexPath: indexPath
            ).render(url)

        case let .title(title):
            return collectionView.dequeueReusableCell(
                for: DetailTitleCell.self,
                   indexPath: indexPath
            ).render(title, appearance: appearance)

        case let .titleDescription(state):
            return collectionView.dequeueReusableCell(
                for: DetailTitleDescriptionCell.self,
                   indexPath: indexPath
            ).render(state, appearance: appearance)

        case let .material(text), let .dimension(text), let .technique(text):
            return collectionView.dequeueReusableCell(
                for: DetailPillCell.self,
                   indexPath: indexPath
            ).render(text, appearance: appearance)

        }
    }

    func collectionView(
        _ collectionView: UICollectionView,
        viewForSupplementaryElementOfKind kindString: String,
        at indexPath: IndexPath
    ) -> UICollectionReusableView {

        let kind = UICollectionViewSupplementaryViewKind(value: kindString)

        guard kind == .header else {
            fatalError()
        }

        let section = self.section(atSectionIndex: indexPath.section)

        return collectionView.dequeueReusableSupplementaryView(
            for: DetailSectionHeaderView.self,
               kind: kind,
               indexPath: indexPath
        ).render(section.sectionTitle ?? "", appearance: appearance)
    }
}
