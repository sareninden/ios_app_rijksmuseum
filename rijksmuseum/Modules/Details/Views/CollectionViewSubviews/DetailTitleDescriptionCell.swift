import UIKit
import ConstraintHelper
import Appearance

extension DetailTitleDescriptionCell {
    struct State: Equatable {
        let title: String
        let description: String
    }
}

final class DetailTitleDescriptionCell: UICollectionViewCell {
    private let titleLabel = UILabel()
    private let descriptionLabel = UILabel()

    override init(frame: CGRect) {
        super.init(frame: frame)

        configureView()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func prepareForReuse() {
        super.prepareForReuse()

        titleLabel.text = nil
        descriptionLabel.text = nil
    }
}

// MARK: - Configure View
extension DetailTitleDescriptionCell {
    private func configureView() {
        createViewHierarchy()
        configureContentView()
        configureTitleLabel()
        configureDescriptionLabel()
    }

    private func createViewHierarchy() {
        [titleLabel, descriptionLabel]
            .activateAutoLayout()
            .add(to: contentView)
    }

    private func configureContentView() {
        contentView.preservesSuperviewLayoutMargins = false
        contentView.insetsLayoutMarginsFromSafeArea = false
    }

    private func configureTitleLabel() {
        titleLabel.pinEdgesToSuperview(
            layoutArea: .layoutMargins,
            excludeEdges: .bottom
        )

        titleLabel.numberOfLines = 0
    }

    private func configureDescriptionLabel() {
        descriptionLabel.pinEdgesToSuperview(
            layoutArea: .layoutMargins,
            excludeEdges: .top
        )

        descriptionLabel.pinBelow(
            view: titleLabel,
            padding: 4
        )

        descriptionLabel.numberOfLines = 0
    }

    override func preferredLayoutAttributesFitting(
        _ layoutAttributes: UICollectionViewLayoutAttributes
    ) -> UICollectionViewLayoutAttributes {

        guard let width = superview?.bounds.width else {
            return layoutAttributes
        }

        let desiredSize = contentView.systemLayoutSizeFitting(
            CGSize(
                width: width,
                height: 1000
            ),
            withHorizontalFittingPriority: .required,
            verticalFittingPriority: .fittingSizeLevel
        )

        layoutAttributes.frame.size = desiredSize
        layoutAttributes.size = desiredSize

        return layoutAttributes
    }
}

// MARK: - Data Driven
extension DetailTitleDescriptionCell {
    @discardableResult
    func render(_ state: State, appearance: Appearance) -> Self {

        contentView.directionalLayoutMargins = appearance.edgeInsets(
            horizontal: .m,
            vertical: .xxs
        )

        titleLabel.text = state.title
        descriptionLabel.text = state.description

        titleLabel.font = appearance.font(for: .title(.title2))
        titleLabel.textColor = appearance.color(for: .foreground(.primary))

        descriptionLabel.font = appearance.font(for: .body(.regular))
        descriptionLabel.textColor = appearance.color(for: .foreground(.secondary))

        return self
    }
}
