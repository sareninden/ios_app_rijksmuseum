import UIKit
import Appearance

final class DetailCloseButton: UIButton {
    override var intrinsicContentSize: CGSize {
        CGSize(width: 44, height: 44)
    }

    convenience init(appearance: Appearance) {
        self.init(type: .close)

        backgroundColor = appearance.color(
            for: .background(.floatingButton)
        )

        layer.masksToBounds = true
    }

    override func layoutSubviews() {
        super.layoutSubviews()

        layer.cornerRadius = 0.5 * min(bounds.width, bounds.height)
    }
}
