import UIKit
import ConstraintHelper
import Appearance

final class DetailTitleCell: UICollectionViewCell {
    private let titleLabel = UILabel()

    override init(frame: CGRect) {
        super.init(frame: frame)

        configureView()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func prepareForReuse() {
        super.prepareForReuse()

        titleLabel.text = nil
    }

    override func preferredLayoutAttributesFitting(
        _ layoutAttributes: UICollectionViewLayoutAttributes
    ) -> UICollectionViewLayoutAttributes {

        guard let width = superview?.bounds.width else {
            return layoutAttributes
        }

        let desiredSize = contentView.systemLayoutSizeFitting(
            CGSize(
                width: width,
                height: 1000
            ),
            withHorizontalFittingPriority: .required,
            verticalFittingPriority: .fittingSizeLevel
        )

        layoutAttributes.frame.size = desiredSize
        layoutAttributes.size = desiredSize

        return layoutAttributes
    }
}

// MARK: - Configure View
extension DetailTitleCell {
    private func configureView() {
        createViewHierarchy()
        configureContentView()
        configureTitleLabel()
    }

    private func createViewHierarchy() {
        [titleLabel]
            .activateAutoLayout()
            .add(to: contentView)
    }

    private func configureContentView() {
        contentView.preservesSuperviewLayoutMargins = false
        contentView.insetsLayoutMarginsFromSafeArea = false
    }

    private func configureTitleLabel() {
        titleLabel.pinEdgesToSuperview(layoutArea: .layoutMargins)

        setContentHuggingPriority(.required, for: .vertical)
        setContentCompressionResistancePriority(.required, for: .vertical)

        titleLabel.textAlignment = .center
        titleLabel.numberOfLines = 0
        titleLabel.setContentHuggingPriority(.required, for: .vertical)
        titleLabel.setContentCompressionResistancePriority(.required, for: .vertical)
    }
}

// MARK: - Data Driven
extension DetailTitleCell {
    @discardableResult
    func render(_ title: String, appearance: Appearance) -> Self {
        titleLabel.text = title
        titleLabel.font = appearance.font(for: .title(.largeTitle))
        titleLabel.textColor = appearance.color(for: .foreground(.primary))

        directionalLayoutMargins = appearance.edgeInsets(
            default: .m,
            bottom: .xs
        )

        return self
    }
}
