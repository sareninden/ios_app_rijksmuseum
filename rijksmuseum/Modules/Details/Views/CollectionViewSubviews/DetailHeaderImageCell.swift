import UIKit
import ConstraintHelper

final class DetailHeaderImageCell: UICollectionViewCell {
    private let imageView = UIImageView()
    private let activityIndicatorView = UIActivityIndicatorView(style: .medium)

    override init(frame: CGRect) {
        super.init(frame: frame)

        configureView()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func prepareForReuse() {
        super.prepareForReuse()

        imageView.cancelImageLoading()
        imageView.image = nil
    }

    override func preferredLayoutAttributesFitting(
        _ layoutAttributes: UICollectionViewLayoutAttributes
    ) -> UICollectionViewLayoutAttributes {

        guard let width = superview?.bounds.width else {
            return layoutAttributes
        }

        let size = CGSize(
            width: width,
            height: floor(0.66 * width)
        )

        layoutAttributes.frame.size = size
        layoutAttributes.size = size

        return layoutAttributes
    }
}

// MARK: - Configure View
extension DetailHeaderImageCell {
    private func configureView() {
        createViewHierarchy()
        configureContentView()
        configureImageView()
        configureActivityIndicatorView()
    }

    private func configureContentView() {
        contentView.preservesSuperviewLayoutMargins = false
        contentView.insetsLayoutMarginsFromSafeArea = false
    }

    private func createViewHierarchy() {
        [imageView, activityIndicatorView]
            .activateAutoLayout()
            .add(to: contentView)
    }

    private func configureImageView() {
        imageView.pinEdgesToSuperview()
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
    }

    private func configureActivityIndicatorView() {
        activityIndicatorView.pinCenterToSuperview()
    }
}

// MARK: - Data Driven
extension DetailHeaderImageCell {
    @discardableResult
    func render(_ url: URL) -> Self {
        setLoading(true)

        if imageView.bounds.width == 0 {
            layoutIfNeeded()
        }

        imageView.load(url, imageSize: bounds.size) { [weak self] _ in
            self?.setLoading(false)
        }

        return self
    }

    private func setLoading(_ isLoading: Bool) {
        activityIndicatorView.isHidden = isLoading == false

        if isLoading {
            activityIndicatorView.startAnimating()
        } else {
            activityIndicatorView.stopAnimating()
        }
    }
}
