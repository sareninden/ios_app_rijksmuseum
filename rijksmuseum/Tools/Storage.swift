import Foundation

/**
 A wrapper protocol around many `UserDefaults` functions making it easier to use other storage solutions later or mock it for testing
 */
public protocol Storage: AnyObject {

    func set(_ value: Any?, forKey key: String)
    func set(_ value: Int, forKey key: String)
    func set(_ value: Float, forKey key: String)
    func set(_ value: Double, forKey key: String)
    func set(_ value: Bool, forKey key: String)

    func removeObject(forKey key: String)

    func object(forKey key: String) -> Any?
    func string(forKey key: String) -> String?
    func integer(forKey key: String) -> Int
    func float(forKey key: String) -> Float
    func double(forKey key: String) -> Double
    func bool(forKey key: String) -> Bool
}

extension UserDefaults: Storage {}
