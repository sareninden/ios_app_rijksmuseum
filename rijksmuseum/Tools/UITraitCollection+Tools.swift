import UIKit

extension UITraitCollection {
    func value<T>(
        pad: @autoclosure () -> T,
        default: @autoclosure () -> T
    ) -> T {
        if userInterfaceIdiom == .pad {
            return pad()
        } else {
            return `default`()
        }
    }
    func value<T>(
        regular: @autoclosure () -> T,
        default: @autoclosure () -> T
    ) -> T {
        if horizontalSizeClass == .regular {
            return regular()
        } else {
            return `default`()
        }
    }
}
