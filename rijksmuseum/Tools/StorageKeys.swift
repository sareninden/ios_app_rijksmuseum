//
//  StorageKeys.swift
//  rijksmuseum
//
//  Created by Saren Inden on 11/12/2021.
//

enum StorageKeys: String {
    case language

    var key: String {
        "nl.sareninden." + rawValue
    }
}
