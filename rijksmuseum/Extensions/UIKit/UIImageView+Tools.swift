import Kingfisher
import UIKit

extension UIImageView {

    func load(
        _ url: URL,
        imageSize: CGSize? = nil,
        completion: ((Bool) -> Void)? = nil
    ) {
        var options: KingfisherOptionsInfo = []

        if let imageSize = imageSize, imageSize.width > 0, imageSize.height > 0  {
            options.append(.processor(DownsamplingImageProcessor(size: imageSize)))
        }

        kf.setImage(
            with: url,
            placeholder: nil,
            options: options
        ) { result in
            switch result {
            case .success:
                completion?(true)
            case .failure:
                completion?(false)
            }
        }
    }

    func cancelImageLoading() {
        kf.cancelDownloadTask()
    }
}
