import RijksMuseumApi

extension ApiLanguage {
    var flagText: String {
        switch self {
        case .en: return "🇺🇸"
        case .nl: return "🇳🇱"
#if DEBUG
        case .in: return "🏁"
#endif
        }
    }
}
