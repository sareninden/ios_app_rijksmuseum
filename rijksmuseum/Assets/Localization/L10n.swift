import Foundation

enum L10n {
    enum MainOverview {}
    enum Details {}
}

// MARK: - MainOverview
extension L10n.MainOverview {
    static var title: String { "Art collections" }
    static var emptyTitle: String { "Nothing found" }
    static var emptyMessage: String { "Your search query has not yielded any results" }
    static var emptyAction: String { "Retry" }

    static var errorTitle: String { "Something went wrong" }
    static var errorMessage: String { "There seems to be an issue with your internet connection or the server. Please try again." }
    static var errorAction: String { "Retry" }

    static var searchPlaceholder: String { "Search in collection" }

    static func sectionHeader(forPage page: Int) -> String {
        String(format: "Page %d", page)
    }
}

// MARK: - Details
extension L10n.Details {
    static var itemLongTitle: String { "Long title" }
    static var itemOtherTitle: String { "Other title" }
    static var itemDescriptionTitle: String { "Description" }

    static var sectionTitleMaterials: String { "Materials" }
    static var sectionTitleTechniques: String { "Techniques" }
    static var sectionTitleDimensions: String { "Dimensions" }
}
