//
//  MainOverviewViewModelTests.swift
//  rijksmuseumTests
//
//  Created by Saren Inden on 11/12/2021.
//

import Foundation
import XCTest
@testable import rijksmuseum
@testable import RijksMuseumApi

final class MainOverviewViewModelTests: XCTestCase {}

// MARK: - Test Clean Load
extension MainOverviewViewModelTests {
    func test_cleanLoad_error_setsStateToError() {
        // Given
        let environment = MainOverviewEnvironmentWrapper()
        let initialState = MainOverviewViewModel.State.initial(
            resultsPerPage: 10,
            language: .en
        )
        var expectedState = initialState
        expectedState.contentState = .error

        let sut = MainOverviewViewModel(
            initialState: initialState,
            environment: environment.newEnvironment()
        )

        // When
        sut.perform(.cleanLoad)

        // Then
        XCTAssertEqual(sut.state, expectedState)
    }

    func test_cleanLoad_successful_setStateContent() throws {
        // Given
        let data = try XCTUnwrap(Bundle.json(named: "ContentRequestData"))
        let decoded = try XCTUnwrap(
            JSONDecoder().decode(CollectionResponse.self, from: data)
        )

        let environment = MainOverviewEnvironmentWrapper()
        environment.apiClient.mockData = data
        let initialState = MainOverviewViewModel.State.initial(
            resultsPerPage: 10,
            language: .en
        )
        var expectedState = initialState
        expectedState.nextPage = 2
        expectedState.contentState = .content(
            MainOverviewViewModel.Content(
                items: decoded.artObjects,
                count: decoded.count
            )
        )

        let sut = MainOverviewViewModel(
            initialState: initialState,
            environment: environment.newEnvironment()
        )

        // When
        sut.perform(.cleanLoad)

        // Then
        XCTAssertEqual(sut.state, expectedState)
    }
}

// MARK: - selected
extension MainOverviewViewModelTests {
    func test_selected_callPresentDetailsScene() {
        // Given
        let environment = MainOverviewEnvironmentWrapper()
        let initialState = MainOverviewViewModel.State.initial(
            resultsPerPage: 10,
            language: .en
        )

        let artObject = ArtObject(
            id: "a",
            objectNumber: "a",
            title: "a",
            longTitle: "a",
            hasImage: true,
            url: nil,
            webImage: nil,
            headerImage: nil
        )
        var presentScenes: [MainOverviewScene] = []
        let expectedState = initialState

        let sut = MainOverviewViewModel(
            initialState: initialState,
            environment: environment.newEnvironment()
        )

        sut.presentScene = {
            presentScenes.append($0)
        }

        // When
        sut.perform(.selected(artObject))

        // Then
        XCTAssertEqual(sut.state, expectedState)
        XCTAssertEqual(presentScenes, [.details(artObject)])
    }
}

// MARK: - set language
extension MainOverviewViewModelTests {
    func test_setLanguage_updateLanguage() {
        // Given
        let languages: [ApiLanguage] = [.en, .nl]
        let environment = MainOverviewEnvironmentWrapper()
        let initialState = MainOverviewViewModel.State.initial(
            resultsPerPage: 10,
            language: .en,
            supportedLanguages: languages
        )

        var expectedState = initialState
        expectedState.language = .nl
        expectedState.contentState = .error

        let sut = MainOverviewViewModel(
            initialState: initialState,
            environment: environment.newEnvironment()
        )

        // When
        sut.perform(.setLanguage(index: 1))

        // Then
        XCTAssertEqual(sut.state, expectedState)
    }
}
