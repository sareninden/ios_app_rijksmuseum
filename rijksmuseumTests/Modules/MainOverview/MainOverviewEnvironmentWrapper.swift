@testable import rijksmuseum
import RijksMuseumApi
import Appearance

final class MainOverviewEnvironmentWrapper {

    let apiClient = APIClientMock()
    let storage = DictionaryStorage()
    let appearance = AppearanceConcrete()

    func newEnvironment() -> MainOverviewEnvironment {
        MainOverviewEnvironment(
            apiClient: apiClient,
            appearance: appearance,
            storage: storage
        )
    }
}
