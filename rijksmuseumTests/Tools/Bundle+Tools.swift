import Foundation
import XCTest

private final class Reference {}

extension Bundle {

    static var current: Bundle { Bundle(for: Reference.self) }

    static func json(named: String) throws -> Data {
        let url = try XCTUnwrap(
            current.url(
                forResource: named,
                withExtension: "json"
            )
        )

        return try XCTUnwrap(Data(contentsOf: url))
    }
}
