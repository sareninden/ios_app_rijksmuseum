import rijksmuseum

/**
 A storage object that uses a dictionary to stored its values. Great for unit testing.
 */
public final class DictionaryStorage: Storage {
    public var content: [String: Any] = [:]

    public init(content: [String: Any] = [:]) {
        self.content = content
    }

    public func set(_ value: Any?, forKey defaultName: String) {
        content[defaultName] = value
    }

    public func set(_ value: Int, forKey defaultName: String) {
        content[defaultName] = value
    }

    public func set(_ value: Float, forKey defaultName: String) {
        content[defaultName] = value
    }

    public func set(_ value: Double, forKey defaultName: String) {
        content[defaultName] = value
    }

    public func set(_ value: Bool, forKey defaultName: String) {
        content[defaultName] = value
    }

    public func removeObject(forKey defaultName: String) {
        content[defaultName] = nil
    }

    public func object(forKey defaultName: String) -> Any? {
        content[defaultName]
    }

    public func string(forKey defaultName: String) -> String? {
        content[defaultName] as? String
    }

    public func integer(forKey defaultName: String) -> Int {
        content[defaultName] as? Int ?? 0
    }

    public func float(forKey defaultName: String) -> Float {
        content[defaultName] as? Float ?? 0
    }

    public func double(forKey defaultName: String) -> Double {
        content[defaultName] as? Double ?? 0
    }

    public func bool(forKey defaultName: String) -> Bool {
        content[defaultName] as? Bool ?? false
    }
}
