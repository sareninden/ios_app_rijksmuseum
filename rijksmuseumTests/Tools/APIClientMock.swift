import RijksMuseumApi
import Foundation

final class APIClientMock: APIClient {

    struct MockError: Error {}

    var language: ApiLanguage = .en
    var mockData: Data?

    func execute<Response: Decodable>(
        request: APIRequest,
        completion: @escaping (Result<Response, Error>) -> Void
    ) throws {

        guard
            let data = mockData,
            let result = try? JSONDecoder().decode(Response.self, from: data)
        else {
            completion(.failure(MockError()))
            return
        }

        completion(.success(result))
    }
}
