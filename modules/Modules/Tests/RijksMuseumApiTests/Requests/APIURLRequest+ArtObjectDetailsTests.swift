import XCTest
import RijksMuseumApi

final class APIURLRequest_ArtObjectDetailsTests: XCTestCase {

    func test_urlRequest() throws {
        // Given
        let objectNumber = "12"
        let rootUrl = try XCTUnwrap(URL(string: "www.test.com"))
        let expectedUrl = rootUrl
            .appendingPathComponent("collection")
            .appendingPathComponent(objectNumber)
        var expectedRequest = URLRequest(url: expectedUrl)
        expectedRequest.httpMethod = "GET"
        expectedRequest.timeoutInterval = 15
        expectedRequest.addValue("application/json", forHTTPHeaderField: "accept")
        expectedRequest.addValue("application/json; charset=UTF-8", forHTTPHeaderField: "content-type")

        // When
        let apiRequest = APIURLRequest.artObjectDetails(objectNumber: objectNumber)
        let urlRequest = try apiRequest.urlRequest(
            for: rootUrl,
               additionalQueryItems: []
        )

        // Expected
        XCTAssertEqual(urlRequest, expectedRequest)
    }
}
