import SwiftUI

public enum SDShadow {
    case light, medium, heavy
    
    var radius: CGFloat {
        switch self {
        case .light: return 6
        case .medium: return 6
        case .heavy: return 5
        }
    }
    
    var color: UIColor {
        switch self {
        case .light: return UIColor.black.withAlphaComponent(0.3)
        case .medium: return UIColor.black.withAlphaComponent(0.4)
        case .heavy: return UIColor.black.withAlphaComponent(0.5)
        }
    }
}
