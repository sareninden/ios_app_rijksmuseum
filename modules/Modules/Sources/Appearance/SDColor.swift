import UIKit

public enum SDColor {
    case background(Background)
    case border(Border)
    case foreground(Foreground)
    case tint(Tint)
    
    public var value: UIColor {

        switch self {
        case let .background(background):
            return background.value
        case let .border(border):
            return border.value
        case let .foreground(foreground):
            return foreground.value
        case let .tint(tint):
            return tint.value
        }
    }
}

// MARK: - Background
extension SDColor {
    public enum Background {
        case primary
        case selectedPrimary
        case floatingButton
        case pill

        var value: UIColor {
            switch self {
            case .primary: return .systemBackground
            case .selectedPrimary: return UIColor.systemGray6
            case .floatingButton: return UIColor.systemGray5
            case .pill:
                return UIColor.init {
                    $0.userInterfaceStyle == .dark ? .systemGray4 : .systemGray6
                }
            }
        }
    }
}

// MARK: - Foreground
extension SDColor {
    public enum Foreground {
        case primary
        case secondary
        case tint

        var value: UIColor {
            switch self {
            case .primary: return .label
            case .secondary: return .secondaryLabel
            case .tint: return .systemBlue
            }
        }
    }
}

// MARK: - Border
extension SDColor {
    public enum Border {
        case regular

        var value: UIColor {
            switch self {
            case .regular:
                return .separator
            }
        }
    }
}

// MARK: - Tint
extension SDColor {
    public enum Tint {
        case primary

        var value: UIColor {
            switch self {
            case .primary:
                return .systemBlue
            }
        }
    }
}
