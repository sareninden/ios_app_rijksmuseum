import SwiftUI

public enum SDFont: Equatable {
    case title(Title)
    case body(Body)
    
    var font: UIFont {
        switch self {
        case let .title(title): return title.font
        case let .body(body): return body.font
        }
    }
}

// MARK: - Title
extension SDFont {
    public enum Title {
        case largeTitle
        case title1
        case title2
        case title3

        fileprivate var font: UIFont {

            switch self {
            case .largeTitle:
                return .preferredFont(forTextStyle: .largeTitle)
            case .title1:
                return .preferredFont(forTextStyle: .title1)
            case .title2:
                return .preferredFont(forTextStyle: .title2)
            case .title3:
                return .preferredFont(forTextStyle: .title3)
            }
        }
    }
}

// MARK: - Body
extension SDFont {
    public enum Body {
        case regular
        case regularMedium
        case regularBold
        case small
        
        fileprivate var font: UIFont {

            switch self {
            case .regular:
                return UIFontMetrics(
                    forTextStyle: .body
                ).scaledFont(
                    for: .systemFont(ofSize: 17, weight: .regular)
                )
            case .regularMedium:
                return UIFontMetrics(
                    forTextStyle: .body
                ).scaledFont(
                    for: .systemFont(ofSize: 17, weight: .medium)
                )
            case .regularBold:
                return UIFontMetrics(
                    forTextStyle: .body
                ).scaledFont(
                    for: .systemFont(ofSize: 17, weight: .bold)
                )
            case .small:
                return .preferredFont(forTextStyle: .caption1)
            }
        }
    }
}
