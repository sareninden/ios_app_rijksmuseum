import CoreGraphics

public enum SDCornerRadius {
    case small, standard, large
    
    var radius: CGFloat {
        switch self {
        /// 2pt
        case .small: return 2
        /// 4pt
        case .standard: return 4
        // 8pt
        case .large: return 8
        }
    }
}
