import UIKit

public protocol Appearance {
    func color(for color: SDColor) -> UIColor
    func spacing(for spacing: SDSpacing) -> CGFloat
    func border(for border: SDBorder) -> CGFloat
    func icon(for icon: SDIcon) -> UIImage
    func font(for font: SDFont) -> UIFont
    func shadowColor(for shadow: SDShadow) -> UIColor
    func shadowRadius(for shadow: SDShadow) -> CGFloat
    func cornerRadius(for corner: SDCornerRadius) -> CGFloat
}

public struct AppearanceConcrete: Appearance {

    public func color(for color: SDColor) -> UIColor { color.value }
    public func spacing(for spacing: SDSpacing) -> CGFloat { spacing.value }
    public func border(for border: SDBorder) -> CGFloat { border.value }
    public func font(for font: SDFont) -> UIFont { font.font }
    public func shadowRadius(for shadow: SDShadow) -> CGFloat { shadow.radius }
    public func shadowColor(for shadow: SDShadow) -> UIColor { shadow.color }
    public func cornerRadius(for corner: SDCornerRadius) -> CGFloat { corner.radius }
    public func icon(for icon: SDIcon) -> UIImage { icon.image }

    public init() {}
}

extension Appearance {
    func spacing(forOptional spacing: SDSpacing?) -> CGFloat {
        guard let spacing = spacing else { return 0 }
        
        return self.spacing(for: spacing)
    }
}
