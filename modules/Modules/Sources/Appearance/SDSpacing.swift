import CoreGraphics

public enum SDSpacing {
    /// 2
    case xxxs
    /// 4
    case xxs
    /// 8
    case xs
    /// 12
    case s
    /// 16
    case m
    /// 24
    case l
    /// 32
    case xl
    /// 40
    case xxl
    /// 48
    case xxxl
    /// 56
    case xxxxl
    
    var value: CGFloat {
        switch self {
        case .xxxs: return 2
        case .xxs: return 4
        case .xs: return 8
        case .s: return 12
        case .m: return 16
        case .l: return 24
        case .xl: return 32
        case .xxl: return 40
        case .xxxl: return 48
        case .xxxxl: return 56
        }
    }
}
