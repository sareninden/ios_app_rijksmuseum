import UIKit

public enum SDBorder {
    case system
    /// 1
    case normal
    /// 2
    case thick
    
    var value: CGFloat {
        switch self {
        case .system: return 1 / max(1, UITraitCollection.current.displayScale)
        case .normal: return 1
        case .thick: return 2
        }
    }
}
