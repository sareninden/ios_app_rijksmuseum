import UIKit

extension Appearance {
    public func edgeInsets(
        default: SDSpacing? = nil,
        horizontal: SDSpacing? = nil,
        vertical: SDSpacing? = nil,
        top: SDSpacing? = nil,
        leading: SDSpacing? = nil,
        bottom: SDSpacing? = nil,
        trailing: SDSpacing? = nil
    ) -> NSDirectionalEdgeInsets {
        NSDirectionalEdgeInsets(
            top: top ?? vertical ?? `default`,
            leading: leading ?? horizontal ?? `default`,
            bottom: bottom ?? vertical ?? `default`,
            trailing: trailing ?? horizontal ?? `default`,
            appearance: self
        )
    }
}

extension NSDirectionalEdgeInsets {
    fileprivate init(
        top: SDSpacing?,
        leading: SDSpacing?,
        bottom: SDSpacing?,
        trailing: SDSpacing?,
        appearance: Appearance
    ) {
        self.init(
            top: appearance.spacing(forOptional: top),
            leading: appearance.spacing(forOptional: leading),
            bottom: appearance.spacing(forOptional: bottom),
            trailing: appearance.spacing(forOptional: trailing)
        )
    }
}
