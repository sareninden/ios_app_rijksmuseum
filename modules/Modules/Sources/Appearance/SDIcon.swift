import UIKit

public enum SDIcon: Equatable {
    case close
}

// MARK: - Mode and image
extension SDIcon {
    var mode: Mode {
        switch self {
        case .close:
            return Mode.system("xmark")
        }
    }
    
    var image: UIImage {
        switch mode {
        case let .asset(name):
            return UIImage(named: name) ?? UIImage()
        case let .system(name):
            return UIImage(systemName: name) ?? UIImage()
        }
    }
}

extension SDIcon {
    public enum Mode: Equatable {
        case asset(String)
        case system(String)
    }
}
