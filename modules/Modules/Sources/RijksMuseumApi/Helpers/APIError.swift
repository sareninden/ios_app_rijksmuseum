public enum APIError: Error {

    case couldNotCreateURLRequest
    case unknown
}
