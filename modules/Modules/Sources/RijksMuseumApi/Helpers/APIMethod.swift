//
//  File.swift
//  
//
//  Created by Saren Inden on 10/12/2021.
//

public enum APIMethod: Equatable {

    case get

    var method: String {

        switch self {
        case .get: return "GET"
        }
    }
}
