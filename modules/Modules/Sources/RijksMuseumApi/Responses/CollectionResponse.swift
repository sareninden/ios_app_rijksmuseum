import Foundation

public struct CollectionResponse: Codable, Equatable {
    public let count: Int
    public let artObjects: [ArtObject]
}
