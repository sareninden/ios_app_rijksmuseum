public struct ArtObjectDetailsResponse: Codable, Equatable {
    public let artObject: ArtObjectDetails
}
