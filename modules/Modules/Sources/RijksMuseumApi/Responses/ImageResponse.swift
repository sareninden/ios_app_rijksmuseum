public struct ImageResponse: Codable, Hashable {
    public let guid: String
    public let width: Int
    public let height: Int
    public let url: String
}
