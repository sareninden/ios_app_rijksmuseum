public struct ArtObject: Codable, Hashable, Identifiable {
    public let id: String
    public let objectNumber: String
    public let title: String
    public let longTitle: String
    public let hasImage: Bool
    public let url: String?

    public let webImage: ImageResponse?
    public let headerImage: ImageResponse?
}
