public struct ArtObjectDetails: Codable, Equatable {
    public let id: String
    public let title: String
    public let titles: [String]
    public let description: String?
    public let principalMaker: String?
    public let materials: [String]?
    public let techniques: [String]?
    public let dimensions: [ArtObjectDimension]?
    public let longTitle: String
    public let hasImage: Bool
    public let url: String?
    public let physicalMedium: String?
    public let scLabelLine: String

    public let webImage: ImageResponse?
    public let headerImage: ImageResponse?

}
