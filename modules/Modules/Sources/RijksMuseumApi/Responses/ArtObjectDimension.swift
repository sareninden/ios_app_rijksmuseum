public struct ArtObjectDimension: Codable, Equatable {
    public let unit: String
    public let type: String
    public let value: String
    public let part: String?
}
