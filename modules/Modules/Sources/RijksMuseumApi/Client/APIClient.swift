//
//  File.swift
//  
//
//  Created by Saren Inden on 10/12/2021.
//

import Foundation
import Combine

public protocol APIClient: AnyObject {
    var language: ApiLanguage { get set }

    func execute<Response: Decodable>(
        request: APIRequest,
        completion: @escaping (Result<Response, Error>) -> Void
    ) throws
}

extension APIClient {

    public func execute<Response: Decodable>(
        resultType: Response.Type,
        request: APIRequest,
        completion: @escaping (Result<Response, Error>) -> Void
    ) throws {
        try execute(request: request, completion: completion)
    }
}
