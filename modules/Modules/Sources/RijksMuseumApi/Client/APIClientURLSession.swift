import Foundation

public final class APIClientURLSession: APIClient {

    private let baseURL: URL
    private let apiToken: String
    private let urlSession: URLSession

    public var language: ApiLanguage = .en

    public init(
        baseURL: URL,
        apiToken: String,
        urlSession: URLSession = .shared
    ) {
        self.baseURL = baseURL
        self.apiToken = apiToken
        self.urlSession = urlSession
    }

    private var baseUrlWithLanguagePath: URL {
        baseURL.appendingPathComponent(language.pathExtension)
    }

    public func execute<Response>(
        request: APIRequest,
        completion: @escaping (Result<Response, Error>) -> Void
    ) throws where Response : Decodable {

        let urlRequest = try request.urlRequest(
            for: baseUrlWithLanguagePath,
               additionalQueryItems: [URLQueryItem(name: "key", value: apiToken)]
        )

        let dataTask = urlSession.dataTask(
            with: urlRequest
        ) { data, response, error in

            print(urlRequest.cURL(pretty: true))

            guard
                let data = data,
                let httpResponse = response as? HTTPURLResponse,
                  (200..<300).contains(httpResponse.statusCode)
            else {
                completion(.failure(error ?? APIError.unknown))
                return
            }

            do {
                let result = try JSONDecoder().decode(Response.self, from: data)

                completion(.success(result))
            } catch {
                print("APIClientUrlSession decode error: \(error)")
                completion(.failure(error))
            }
        }

        dataTask.resume()
    }
}

