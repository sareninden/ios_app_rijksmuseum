public enum ApiLanguage: String, Equatable, CaseIterable {
    case nl, en

#if DEBUG
    case `in`
#endif

    var pathExtension: String {
        switch self {
        case .nl: return "nl"
        case .en: return "en"
#if DEBUG
        case .in: return "in"
#endif
        }
    }
}
