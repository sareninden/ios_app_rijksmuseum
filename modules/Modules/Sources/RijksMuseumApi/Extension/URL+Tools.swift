import Foundation

extension URL {

    init(baseURL: URL, pathComponents: [String]) {

        var result = baseURL

        pathComponents.forEach {
            result.appendPathComponent($0)
        }

        self = result
    }
}
