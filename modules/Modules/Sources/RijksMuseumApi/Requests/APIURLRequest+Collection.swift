import Foundation

extension APIURLRequest {
    public static func collections(
        query: String? = nil,
        involvedMaker: String? = nil,
        pageOffset: Int?,
        resultsPerPage: Int? = nil
    ) -> APIRequest {

        var queryItems: [URLQueryItem] = []

        if let query = query, query.isEmpty == false {
            queryItems.append(.init(name: "q", value: query))
        }

        if let involvedMaker = involvedMaker {
            queryItems.append(.init(name: "involvedMaker", value: involvedMaker))
        }

        if let pageOffset = pageOffset {
            queryItems.append(.init(name: "p", value: "\(pageOffset)"))
        }

        if let resultsPerPage = resultsPerPage {
            queryItems.append(.init(name: "ps", value: "\(resultsPerPage)"))
        }

        return APIURLRequest(
            pathComponents: ["collection"],
            queryItems: queryItems
        )
    }
}
