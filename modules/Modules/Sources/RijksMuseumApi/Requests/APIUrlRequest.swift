import Foundation

public struct APIURLRequest: APIRequest, Equatable {

    public let method: APIMethod
    public let pathComponents: [String]
    public let queryItems: [URLQueryItem]
    public let timeout: TimeInterval?

    public init(
        method: APIMethod = .get,
        pathComponents: [String] = [],
        queryItems: [URLQueryItem] = [],
        timeout: TimeInterval? = 15
    ) {
        self.method = method
        self.pathComponents = pathComponents
        self.queryItems = queryItems
        self.timeout = timeout
    }

    public func urlRequest(
        for baseURL: URL,
        additionalQueryItems: [URLQueryItem]
    ) throws -> URLRequest {

        guard var components = URLComponents(
            url: URL(baseURL: baseURL, pathComponents: pathComponents),
            resolvingAgainstBaseURL: false
        ) else {
            throw APIError.couldNotCreateURLRequest
        }

        if queryItems.isEmpty == false
            || additionalQueryItems.isEmpty == false {
            components.queryItems = queryItems + additionalQueryItems
        }

        guard let url = components.url else {
            throw APIError.couldNotCreateURLRequest
        }

        var request = URLRequest(url: url)

        request.httpMethod = method.method

        if let timeout = timeout {
            request.timeoutInterval = timeout
        }

        request.addValue("application/json", forHTTPHeaderField: "accept")
        request.addValue("application/json; charset=UTF-8", forHTTPHeaderField: "content-type")

        return request
    }
}
