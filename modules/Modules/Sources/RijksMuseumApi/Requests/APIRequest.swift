//
//  File.swift
//  
//
//  Created by Saren Inden on 10/12/2021.
//

import Foundation

public protocol APIRequest {

    var method: APIMethod { get }

    func urlRequest(
        for baseURL: URL,
        additionalQueryItems: [URLQueryItem]
    ) throws -> URLRequest
}
