extension APIURLRequest {
    public static func artObjectDetails(
        objectNumber: String
    ) -> APIRequest {
        APIURLRequest(
            pathComponents: ["collection", objectNumber],
            queryItems: []
        )
    }
}
