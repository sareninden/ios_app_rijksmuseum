import UIKit

extension Collection where Element == UIView {
    /**
     sets the property `translatesAutoresizingMaskIntoConstraints` of all views in the array to false
     */
    @discardableResult
    public func activateAutoLayout() -> Self {
        forEach { $0.translatesAutoresizingMaskIntoConstraints = false }

        return self
    }

    /**
     Adds all the views in the collection to the given view
     - Parameter view: the view in which the views will be added
     */
    @discardableResult
    public func add(to view: UIView) -> Self {
        forEach(view.addSubview)

        return self
    }
}
