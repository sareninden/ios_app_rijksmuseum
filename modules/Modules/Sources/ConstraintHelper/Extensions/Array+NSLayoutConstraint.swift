import UIKit

extension Array where Element == NSLayoutConstraint {
    /**
     Set the priority for all the constraints
     */
    @inlinable
    @discardableResult
    public func setPriority(_ priority: UILayoutPriority) -> [Element] {
        forEach { $0.priority = priority }
        return self
    }

    /**
     Set the priority for all the constraints
     */
    @inlinable
    @discardableResult
    public func setPriority(_ priority: Float) -> [Element] {
        forEach { $0.priority = UILayoutPriority(priority) }
        return self
    }
    /**
     Activates all the constraints
     */
    @inlinable
    @discardableResult
    public func activateConstraints() -> [Element] {
        NSLayoutConstraint.activate(self)
        return self
    }

    /**
     Deactivates all the constraints
     */
    @inlinable
    @discardableResult
    public func deactivateConstraints() -> [Element] {
        NSLayoutConstraint.deactivate(self)
        return self
    }

    /**
     Set all the constraints to active or not. Returns the instance so multiple function calls can be chained
     - Parameter active: if the constraint should be active or  not
     - Returns: self
     */
    @inlinable
    @discardableResult
    public func set(active: Bool) -> [Element] {
        if active {
            return activateConstraints()
        } else {
            return deactivateConstraints()
        }
    }

    /**
     Filters the array for constraints relating to the given layout anchor
     - Parameter anchor: the anchor which the returned constrains should contain
     - Returns: the current array filtered so that only constraints are included that contain the given anchor
     */
    public func constraintsContaining(_ anchor: NSLayoutAnchor<AnyObject>) -> [Element] {
        filter { $0.firstAnchor == anchor || $0.secondAnchor == anchor }
    }

    /**
     Filters the array for constraints relating to the given layout anchor
     - Parameter anchor: the anchor which the returned constrains should contain
     - Returns: the current array filtered so that only constraints are included that contain the given anchor
     */
    public func constraintsContaining(_ anchor: NSLayoutYAxisAnchor) -> [Element] {
        filter { $0.firstAnchor == anchor || $0.secondAnchor == anchor }
    }

    /**
     Filters the array for constraints relating to the given layout anchor
     - Parameter anchor: the anchor which the returned constrains should contain
     - Returns: the current array filtered so that only constraints are included that contain the given anchor
     */
    public func constraintsContaining(_ anchor: NSLayoutXAxisAnchor) -> [Element] {
        filter { $0.firstAnchor == anchor || $0.secondAnchor == anchor }
    }

    /**
     Filters the array for constraints relating to the given layout anchor
     - Parameter anchor: the anchor which the returned constrains should contain
     - Returns: the current array filtered so that only constraints are included that contain the given anchor
     */
    public func constraintsContaining(_ anchor: NSLayoutDimension) -> [Element] {
        filter { $0.firstAnchor == anchor || $0.secondAnchor == anchor }
    }
}
