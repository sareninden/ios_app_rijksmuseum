import UIKit

extension NSDirectionalEdgeInsets: Sideble {
    public var left: CGFloat {
        get { leading }
        set { leading = newValue }
    }

    public var right: CGFloat {
        get { trailing }
        set { trailing = newValue }
    }

    public init(top: CGFloat, left: CGFloat, bottom: CGFloat, right: CGFloat) {
        self = Self(top: top, leading: left, bottom: bottom, trailing: right)
    }
}
