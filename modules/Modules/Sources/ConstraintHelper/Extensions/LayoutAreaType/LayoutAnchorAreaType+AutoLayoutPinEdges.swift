import UIKit

// MARK: - All Edges
extension LayoutAnchorAreaType {
    /**
     Pin the edges view to the given view.
     - Parameter view: the view to pin this view edges to.
     - Parameter padding: the padding of the edges. The bottom and right (trailing) values will be prefixed with `-`. Defaults to `.zero`.
     - Parameter layoutArea: use the views default anchors, the safe area anchors or the layout margin anchors. Defaults to `.default`.
     - Parameter excludeEdges: the edges that should not be pinned. Defaults to `.none`.
     - Parameter relation: constraints should be equal, lessThanOrEqual or greaterThanOrEqual. The value for trailing and bottom will be flipped. Defaults to `.equal`.
     - Parameter active: enable the constraints, defaults to `true`.

     - Returns: an array with the created constraints.
     */
    @discardableResult
    public func pinEdges(
        to view: UIView,
        padding: NSDirectionalEdgeInsets = .zero,
        layoutArea: UIView.LayoutArea = .default,
        excludeEdges: NSLayoutConstraint.Edge = .none,
        relation: NSLayoutConstraint.Relation = .equal,
        active: Bool = true
    ) -> [NSLayoutConstraint] {
        let relations = NSLayoutConstraint.RelationsAround(
            top: relation,
            leading: relation,
            trailing: relation.flipped,
            bottom: relation.flipped
        )

        return pinEdges(
            to: view.layoutArea(for: layoutArea),
            padding: padding,
            excludeEdges: excludeEdges,
            relations: relations,
            active: active
        )
    }

    /**
     Pin the edges view to the given view.
     - Parameter view: the view to pin this view edges to.
     - Parameter padding: the padding of the edges. The bottom and right (trailing) values will be prefixed with `-`. Defaults to `.zero`.
     - Parameter layoutArea: use the views default anchors, the safe area anchors or the layout margin anchors. Defaults to `.default`.
     - Parameter excludeEdges: the edges that should not be pinned. Defaults to `.none`.
     - Parameter relations: constraints should be equal, lessThanOrEqual or greaterThanOrEqual. The value for trailing and bottom will **not** be flipped.
     The `relations` do not have a default due to signature conflicts that will arise with the functions that have `relation` as input parameter.
     - Parameter active: enable the constraints, defaults to `true`.

     - Returns: an array with the created constraints.
     */
    @discardableResult
    public func pinEdges(
        to view: UIView,
        padding: NSDirectionalEdgeInsets = .zero,
        layoutArea: UIView.LayoutArea = .default,
        excludeEdges: NSLayoutConstraint.Edge = .none,
        relations: NSLayoutConstraint.RelationsAround,
        active: Bool = true
    ) -> [NSLayoutConstraint] {
        pinEdges(
            to: view.layoutArea(for: layoutArea),
            padding: padding,
            excludeEdges: excludeEdges,
            relations: relations,
            active: active
        )
    }
}

// MARK: - Vertical Edges
extension LayoutAnchorAreaType {
    /**
     Pins the view vertical in the given view.
     - Parameter view: the view to pin this view vertical edges to.
     - Parameter padding: the padding of the edges. Defaults to `0`.
     - Parameter layoutArea: use the views default anchors, the safe area anchors or the layout margin anchors. Defaults to `.default`.
     - Parameter relation: constraints should be equal, lessThanOrEqual or greaterThanOrEqual. The value for bottom will be flipped. Defaults to `.equal`.
     - Parameter active: enable the constraints, defaults to `true`.

     - Returns: an array with the created constraints.
     */
    @discardableResult
    public func pinVerticalEdges(
        to view: UIView,
        padding: CGFloat = 0,
        layoutArea: UIView.LayoutArea = .default,
        relation: NSLayoutConstraint.Relation = .equal,
        active: Bool = true
    ) -> [NSLayoutConstraint] {
        pinEdges(
            to: view.layoutArea(for: layoutArea),
            padding: [.vertical: padding],
            excludeEdges: .horizontal,
            relation: relation,
            active: active
        )
    }
}

// MARK: - Horizontal Edges
extension LayoutAnchorAreaType {
    /**
     Pins the view horizontal in the given view.
     - Parameter view: the view to pin this view horizontal edges to.
     - Parameter padding: the padding of the edges. Defaults to `0`.
     - Parameter layoutArea: use the views default anchors, the safe area anchors or the layout margin anchors. Defaults to `.default`.
     - Parameter relation: constraints should be equal, lessThanOrEqual or greaterThanOrEqual. The value for trailing will be flipped. Defaults to `.equal`.
     - Parameter active: enable the constraints, defaults to `true`.

     - Returns: an array with the created constraints.
     */
    @discardableResult
    public func pinHorizontalEdges(
        to view: UIView,
        padding: CGFloat = 0,
        layoutArea: UIView.LayoutArea = .default,
        relation: NSLayoutConstraint.Relation = .equal,
        active: Bool = true
    ) -> [NSLayoutConstraint] {
        pinEdges(
            to: view.layoutArea(for: layoutArea),
            padding: [.horizontal: padding],
            excludeEdges: .vertical,
            relation: relation,
            active: active
        )
    }
}

// MARK: - Leading top
extension LayoutAnchorAreaType {
    /**
     Pins the view leading and top anchors to the given view.
     - Parameter view: the view to pin to.
     - Parameter padding: the padding of the edges. Defaults to `.zero`.
     - Parameter layoutArea: use the views default anchors, the safe area anchors or the layout margin anchors. Defaults to `.default`.
     - Parameter relation: constraints should be equal, lessThanOrEqual or greaterThanOrEqual. Defaults to `.equal`.
     - Parameter active: enable the constraints, defaults to `true`.

     - Returns: an array with the created constraints.
     */
    @discardableResult
    public func pinLeadingTop(
        to view: UIView,
        padding: NSDirectionalEdgeInsets = .zero,
        layoutArea: UIView.LayoutArea = .default,
        relation: NSLayoutConstraint.Relation = .equal,
        active: Bool = true
    ) -> [NSLayoutConstraint] {
        pinEdges(
            to: view.layoutArea(for: layoutArea),
            padding: padding,
            excludeEdges: [.trailing, .bottom],
            relation: relation,
            active: active
        )
    }
}

// MARK: - Trailing top
extension LayoutAnchorAreaType {
    /**
     Pins the view leading and top anchors to the given view.
     - Parameter view: the view to pin to.
     - Parameter padding: the padding of the edges. Defaults to `.zero`.
     - Parameter layoutArea: use the views default anchors, the safe area anchors or the layout margin anchors. Defaults to `.default`.
     - Parameter relation: constraints should be equal, lessThanOrEqual or greaterThanOrEqual. The value for trailing will be flipped. Defaults to `.equal`.
     - Parameter active: enable the constraints, defaults to `true`.

     - Returns: an array with the created constraints.
     */
    @discardableResult
    public func pinTrailingTop(
        to view: UIView,
        padding: NSDirectionalEdgeInsets = .zero,
        layoutArea: UIView.LayoutArea = .default,
        relation: NSLayoutConstraint.Relation = .equal,
        active: Bool = true
    ) -> [NSLayoutConstraint] {
        pinEdges(
            to: view.layoutArea(for: layoutArea),
            padding: padding,
            excludeEdges: [.leading, .bottom],
            relation: relation,
            active: active
        )
    }
}

// MARK: - Custom controlled edges
extension LayoutAnchorAreaType {
    /**
     Pin the edges view to the given surrounding anchors.
     - Parameter layoutArea: the layout anchors to pin this view edges to.
     - Parameter padding: the padding of the edges. The bottom and right (trailing) values will be prefixed with `-`. Defaults to `.zero`.
     - Parameter excludeEdges: the edges that should not be pinned. Defaults to `.none`.
     - Parameter relation: constraints should be equal, lessThanOrEqual or greaterThanOrEqual. The values for bottom and trailing will be flipped. Defaults to `.equal`.
     - Parameter active: enable the constraints, defaults to `true`.

     - Returns: an array with the created constraints.
     */
    @discardableResult
    public func pinEdges(
        to layoutArea: LayoutAnchorAreaType,
        padding: NSDirectionalEdgeInsets = .zero,
        excludeEdges: NSLayoutConstraint.Edge = .none,
        relation: NSLayoutConstraint.Relation = .equal,
        active: Bool = true
    ) -> [NSLayoutConstraint] {
        let relations = NSLayoutConstraint.RelationsAround(
            top: relation,
            leading: relation,
            trailing: relation.flipped,
            bottom: relation.flipped
        )

        return pinEdges(
            to: layoutArea,
            padding: padding,
            excludeEdges: excludeEdges,
            relations: relations,
            active: active
        )
    }

    /**
     Pin the edges view to the given surrounding anchors.
     - Parameter layoutArea: the layout anchors to pin this view edges to.
     - Parameter padding: the padding of the edges. The bottom and right (trailing) values will be prefixed with `-`. Defaults to `.zero`.
     - Parameter excludeEdges: the edges that should not be pinned. Defaults to `.none`.
     - Parameter relations: constraints should be equal, lessThanOrEqual or greaterThanOrEqual. Trailing and bottom will **not** be flipped.
     The `relations` do not have a default due to signature conflicts that will arise with the functions that have `relation` as input parameter.
     - Parameter active: enable the constraints, defaults to `true`.

     - Returns: an array with the created constraints.
     */
    @discardableResult
    public func pinEdges(
        to layoutArea: LayoutAnchorAreaType,
        padding: NSDirectionalEdgeInsets = .zero,
        excludeEdges: NSLayoutConstraint.Edge = .none,
        relations: NSLayoutConstraint.RelationsAround,
        active: Bool = true
    ) -> [NSLayoutConstraint] {
        var constraints: [NSLayoutConstraint] = []

        if excludeEdges.contains(.top) == false {
            let topConstraint = topAnchor.constraint(
                to: layoutArea.topAnchor,
                constant: padding.top,
                relation: relations.top
            )
            constraints.append(topConstraint)
        }

        if excludeEdges.contains(.leading) == false {
            let leadingConstraint = leadingAnchor.constraint(
                to: layoutArea.leadingAnchor,
                constant: padding.leading,
                relation: relations.leading
            )
            constraints.append(leadingConstraint)
        }

        if excludeEdges.contains(.bottom) == false {
            let bottomConstraint = bottomAnchor.constraint(
                to: layoutArea.bottomAnchor,
                constant: -padding.bottom,
                relation: relations.bottom
            )
            constraints.append(bottomConstraint)
        }

        if excludeEdges.contains(.trailing) == false {
            let trailingConstraint = trailingAnchor.constraint(
                to: layoutArea.trailingAnchor,
                constant: -padding.trailing,
                relation: relations.trailing
            )
            constraints.append(trailingConstraint)
        }

        if active {
            NSLayoutConstraint.activate(constraints)
        }

        return constraints
    }
}
