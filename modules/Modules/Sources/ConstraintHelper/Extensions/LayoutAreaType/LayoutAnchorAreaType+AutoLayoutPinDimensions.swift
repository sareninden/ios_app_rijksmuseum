import UIKit

// MARK: - Size
extension LayoutAnchorAreaType {
    /**
     Pin the view size to the given size
     - Parameter size: the size the view should be
     - Parameter relation: constraints should be equal, lessThanOrEqual or greaterThanOrEqual. Defaults to `.equal`.
     - Parameter active: enable the constraints. Defaults to `true`.
     */
    @discardableResult
    public func pinSize(
        _ size: CGSize,
        relation: NSLayoutConstraint.Relation = .equal,
        active: Bool = true
    ) -> [NSLayoutConstraint] {
        let constraints = [
            widthAnchor.constraint(toConstant: size.width, relation: relation),
            heightAnchor.constraint(toConstant: size.height, relation: relation)
        ]

        if active {
            NSLayoutConstraint.activate(constraints)
        }

        return constraints
    }

    /**
     Pin the view size to the given size
     - Parameter singleSize: the size the view should be (width and height are equal)
     - Parameter relation: constraints should be equal, lessThanOrEqual or greaterThanOrEqual. Defaults to `.equal`.
     - Parameter active: enable the constraints. Defaults to `true`.
     */
    @discardableResult
    public func pinSquared(
        to singleSize: CGFloat,
        relation: NSLayoutConstraint.Relation = .equal,
        active: Bool = true
    ) -> [NSLayoutConstraint] {
        let constraints = [
            widthAnchor.constraint(toConstant: singleSize, relation: relation),
            heightAnchor.constraint(toConstant: singleSize, relation: relation)
        ]

        if active {
            NSLayoutConstraint.activate(constraints)
        }

        return constraints
    }
}

// MARK: - Width
extension LayoutAnchorAreaType {
    /**
     Pin the view width related to the given views width
     - Parameter view: the view where this views width should relate to
     - Parameter multiplier: the multiplier for the width relation.
     - Parameter extraWidth: the extra width that should be added to this view. Defaults to `0`.
     - Parameter layoutArea: use the views default anchors, the safe area anchors or the layout margin anchors. Defaults to `.default`.
     - Parameter relation: constraints should be equal, lessThanOrEqual or greaterThanOrEqual. Defaults to `.equal`.
     - Parameter active: enable the constraints. Defaults to `true`.
     */
    @discardableResult
    public func pinWidth(
        to view: UIView,
        multiplier: CGFloat = 1,
        extraWidth: CGFloat = 0,
        layoutArea: UIView.LayoutArea = .default,
        relation: NSLayoutConstraint.Relation = .equal,
        active: Bool = true
    ) -> NSLayoutConstraint {
        pinWidth(
            to: view.layoutArea(for: layoutArea),
            multiplier: multiplier,
            extraWidth: extraWidth,
            relation: relation,
            active: active
        )
    }

    /**
     Pin the view width related to the given views width
     - Parameter layoutArea: the layout area with which the width needs to be pinned
     - Parameter multiplier: the multiplier for the width relation.
     - Parameter extraWidth: the extra width that should be added to this view. Defaults to `0`.
     - Parameter relation: constraints should be equal, lessThanOrEqual or greaterThanOrEqual. Defaults to `.equal`.
     - Parameter active: enable the constraints. Defaults to `true`.
     */
    @discardableResult
    public func pinWidth(
        to layoutArea: LayoutAnchorAreaType,
        multiplier: CGFloat = 1,
        extraWidth: CGFloat = 0,
        relation: NSLayoutConstraint.Relation = .equal,
        active: Bool = true
    ) -> NSLayoutConstraint {
        widthAnchor.constraint(
            to: layoutArea.widthAnchor,
            multiplier: multiplier,
            constant: extraWidth,
            relation: relation
        ).setActive(active)
    }

    /**
     Pin the view width to the given constant
     - Parameter width: the width that this view should be
     - Parameter relation: constraints should be equal, lessThanOrEqual or greaterThanOrEqual. Defaults to `.equal`.
     - Parameter active: enable the constraints. Defaults to `true`.
     */
    @discardableResult
    public func pinWidth(
        _ width: CGFloat,
        relation: NSLayoutConstraint.Relation = .equal,
        active: Bool = true
    ) -> NSLayoutConstraint {
        widthAnchor.constraint(
            toConstant: width,
            relation: relation
        ).setActive(active)
    }
}

// MARK: - Height
extension LayoutAnchorAreaType {
    /**
     Pin the view height related to the given views height
     - Parameter view: the view where this views height should relate to
     - Parameter multiplier: the multiplier for the height relation. Defaults to `1`.
     - Parameter extraHeight: the extra height that should be added to this view. Defaults to `0`.
     - Parameter layoutArea: use the views default anchors, the safe area anchors or the layout margin anchors. Defaults to `.default`.
     - Parameter relation: constraints should be equal, lessThanOrEqual or greaterThanOrEqual. Defaults to `.equal`.
     - Parameter active: enable the constraints. Defaults to `true`.
     */
    @discardableResult
    public func pinHeight(
        with view: UIView,
        multiplier: CGFloat = 1,
        extraHeight: CGFloat = 0,
        layoutArea: UIView.LayoutArea = .default,
        relation: NSLayoutConstraint.Relation = .equal,
        active: Bool = true
    ) -> NSLayoutConstraint {
        pinHeight(
            with: view.layoutArea(for: layoutArea),
            multiplier: multiplier,
            extraHeight: extraHeight,
            relation: relation,
            active: active
        )
    }

    /**
     Pin the view height related to the given views height
     - Parameter layoutArea: the layout area with which the height needs to be pinned
     - Parameter multiplier: the multiplier for the height relation. Defaults to `1`.
     - Parameter extraHeight: the extra height that should be added to this view. Defaults to `0`.
     - Parameter relation: constraints should be equal, lessThanOrEqual or greaterThanOrEqual. Defaults to `.equal`.
     - Parameter active: enable the constraints. Defaults to `true`.
     */
    @discardableResult
    public func pinHeight(
        with layoutArea: LayoutAnchorAreaType,
        multiplier: CGFloat = 1,
        extraHeight: CGFloat = 0,
        relation: NSLayoutConstraint.Relation = .equal,
        active: Bool = true
    ) -> NSLayoutConstraint {
        heightAnchor.constraint(
            to: layoutArea.heightAnchor,
            multiplier: multiplier,
            constant: extraHeight,
            relation: relation
        ).setActive(active)
    }

    /**
     Pin the view height to the given constant
     - Parameter height: the height that this view should be
     - Parameter relation: constraints should be equal, lessThanOrEqual or greaterThanOrEqual. Defaults to `.equal`.
     - Parameter active: enable the constraints. Defaults to `true`.
     */
    @discardableResult
    public func pinHeight(
        _ height: CGFloat,
        relation: NSLayoutConstraint.Relation = .equal,
        active: Bool = true
    ) -> NSLayoutConstraint {
        heightAnchor.constraint(
            toConstant: height,
            relation: relation
        ).setActive(active)
    }
}
