import UIKit

// MARK: - Top
extension LayoutAnchorAreaType {
    /**
     Pins the top anchor of this view to the given views layout area
     - Parameter view: the view to pin this view top anchor to.
     - Parameter padding: the padding between the two layout anchors. Defaults to `0`.
     - Parameter layoutArea: use the views default anchors, the safe area anchors or the layout margin anchors. Defaults to `.default`.
     - Parameter relation: constraints should be equal, lessThanOrEqual or greaterThanOrEqual. Defaults to `.equal`.
     - Parameter active: enable the constraints. Defaults to `true`.
     - Returns: the created constraint.
     */
    @discardableResult
    public func pinTop(
        to view: UIView,
        padding: CGFloat = 0,
        layoutArea: UIView.LayoutArea = .default,
        relation: NSLayoutConstraint.Relation = .equal,
        active: Bool = true
    ) -> NSLayoutConstraint {
        pinTop(
            to: view.layoutArea(for: layoutArea),
            padding: padding,
            relation: relation,
            active: active
        )
    }

    /**
     Pins the top anchor of this view to the given layout area top anchor
     - Parameter layoutArea: the layout anchors to pin this view top anchor to.
     - Parameter padding: the padding between the two layout anchors. Defaults to `0`.
     - Parameter relation: constraints should be equal, lessThanOrEqual or greaterThanOrEqual. Defaults to `.equal`.
     - Parameter active: enable the constraints. Defaults to `true`.
     - Returns: the created constraint.
     */
    @discardableResult
    public func pinTop(
        to layoutArea: LayoutAnchorAreaType,
        padding: CGFloat = 0,
        relation: NSLayoutConstraint.Relation = .equal,
        active: Bool = true
    ) -> NSLayoutConstraint {
        topAnchor.constraint(
            to: layoutArea.topAnchor,
            constant: padding,
            relation: relation
        ).setActive(active)
    }
}

// MARK: - Leading
extension LayoutAnchorAreaType {
    /**
     Pins the leading anchor of this view to the given views layout area
     - Parameter view: the view to pin this view leading anchor to.
     - Parameter padding: the padding between the two layout anchors. Defaults to `0`.
     - Parameter layoutArea: use the views default anchors, the safe area anchors or the layout margin anchors. Defaults to `.default`.
     - Parameter relation: constraints should be equal, lessThanOrEqual or greaterThanOrEqual. Defaults to `.equal`.
     - Parameter active: enable the constraints. Defaults to `true`.
     - Returns: the created constraint.
     */
    @discardableResult
    public func pinLeading(
        to view: UIView,
        padding: CGFloat = 0,
        layoutArea: UIView.LayoutArea = .default,
        relation: NSLayoutConstraint.Relation = .equal,
        active: Bool = true
    ) -> NSLayoutConstraint {
        pinLeading(
            to: view.layoutArea(for: layoutArea),
            padding: padding,
            relation: relation,
            active: active
        )
    }

    /**
     Pins the leading anchor of this view to the given layout area leading anchor
     - Parameter layoutArea: the layout anchors to pin this view leading anchor to.
     - Parameter padding: the padding between the two layout anchors. Defaults to `0`.
     - Parameter relation: constraints should be equal, lessThanOrEqual or greaterThanOrEqual. Defaults to `.equal`.
     - Parameter active: enable the constraints. Defaults to `true`.
     - Returns: the created constraint.
     */
    @discardableResult
    public func pinLeading(
        to layoutArea: LayoutAnchorAreaType,
        padding: CGFloat = 0,
        relation: NSLayoutConstraint.Relation = .equal,
        active: Bool = true
    ) -> NSLayoutConstraint {
        leadingAnchor.constraint(
            to: layoutArea.leadingAnchor,
            constant: padding,
            relation: relation
        ).setActive(active)
    }
}

// MARK: - Trailing
extension LayoutAnchorAreaType {
    /**
     Pins the trailing anchor of this view to the given views layout area
     - Parameter view: the view to pin this view leading anchor to.
     - Parameter padding: the padding between the two layout anchors. Defaults to `0`.
     - Parameter layoutArea: use the views default anchors, the safe area anchors or the layout margin anchors. Defaults to `.default`.
     - Parameter relation: constraints should be equal, lessThanOrEqual or greaterThanOrEqual. Defaults to `.equal`.
     - Parameter active: enable the constraints. Defaults to `true`.
     - Returns: the created constraint.
     */
    @discardableResult
    public func pinTrailing(
        to view: UIView,
        padding: CGFloat = 0,
        layoutArea: UIView.LayoutArea = .default,
        relation: NSLayoutConstraint.Relation = .equal,
        active: Bool = true
    ) -> NSLayoutConstraint {
        pinTrailing(
            to: view.layoutArea(for: layoutArea),
            padding: padding,
            relation: relation,
            active: active
        )
    }

    /**
     Pins the leading anchor of this view to the given layout area leading anchor
     - Parameter layoutArea: the layout anchors to pin this view leading anchor to.
     - Parameter padding: the padding between the two layout anchors. Defaults to `0`.
     - Parameter relation: constraints should be equal, lessThanOrEqual or greaterThanOrEqual. Defaults to `.equal`.
     - Parameter active: enable the constraints. Defaults to `true`.
     - Returns: the created constraint.
     */
    @discardableResult
    public func pinTrailing(
        to layoutArea: LayoutAnchorAreaType,
        padding: CGFloat = 0,
        relation: NSLayoutConstraint.Relation = .equal,
        active: Bool = true
    ) -> NSLayoutConstraint {
        trailingAnchor.constraint(
            to: layoutArea.trailingAnchor,
            constant: -padding,
            relation: relation.flipped
        ).setActive(active)
    }
}

// MARK: - Bottom
extension LayoutAnchorAreaType {
    /**
     Pins the bottom anchor of this view to the given views layout area
     - Parameter view: the view to pin this view bottom anchor to
     - Parameter padding: the padding between the two layout anchors. Defaults to `0`.
     - Parameter layoutArea: use the views default anchors, the safe area anchors or the layout margin anchors. Defaults to `.default`.
     - Parameter relation: constraints should be equal, lessThanOrEqual or greaterThanOrEqual. Defaults to `.equal`.
     - Parameter active: enable the constraints. Defaults to `true`.
     - Returns: the created constraint.
     */
    @discardableResult
    public func pinBottom(
        to view: UIView,
        padding: CGFloat = 0,
        layoutArea: UIView.LayoutArea = .default,
        relation: NSLayoutConstraint.Relation = .equal,
        active: Bool = true
    ) -> NSLayoutConstraint {
        pinBottom(
            to: view.layoutArea(for: layoutArea),
            padding: padding,
            relation: relation,
            active: active
        )
    }

    /**
     Pins the leading anchor of this view to the given layout area leading anchor
     - Parameter layoutArea: the layout anchors to pin this view bottom anchor to
     - Parameter padding: the padding between the two layout anchors. This values sign is flipped when creating the constraint.  Defaults to `0`.
     - Parameter relation: constraints should be equal, lessThanOrEqual or greaterThanOrEqual. Defaults to `.equal`.
     - Parameter active: enable the constraints. Defaults to `true`.
     - Returns: the created constraint.
     */
    @discardableResult
    public func pinBottom(
        to layoutArea: LayoutAnchorAreaType,
        padding: CGFloat = 0,
        relation: NSLayoutConstraint.Relation = .equal,
        active: Bool = true
    ) -> NSLayoutConstraint {
        bottomAnchor.constraint(
            to: layoutArea.bottomAnchor,
            constant: -padding,
            relation: relation.flipped
        ).setActive(active)
    }
}
