import UIKit

// MARK: - Complete Center
extension LayoutAnchorAreaType {
    /**
     Pin the view center to the given views center
     - Parameter view: the view to which this views center should relate to
     - Parameter offset: the offset from the given views center
     - Parameter layoutArea: use the views default anchors, the safe area anchors or the layout margin anchors. Defaults to `.default`.
     - Parameter active: enable the constraints. Defaults to `true`.
     - Returns: the created constraints
     */
    @discardableResult
    public func pinCenter(
        to view: UIView,
        offset: CGPoint = .zero,
        layoutArea: UIView.LayoutArea = .default,
        active: Bool = true
    ) -> [NSLayoutConstraint] {
        pinCenter(
            to: view.layoutArea(for: layoutArea),
            offset: offset,
            active: active
        )
    }

    /**
     Pin the view center to the given layout area center
     - Parameter view: the view to which this views center should relate to
     - Parameter offset: the offset from the given views center
     - Parameter active: enable the constraints. Defaults to `true`.
     - Returns: the created constraints
     */
    @discardableResult
    public func pinCenter(
        to layoutArea: LayoutAnchorAreaType,
        offset: CGPoint = .zero,
        active: Bool = true
    ) -> [NSLayoutConstraint] {
        let constraints = [
            centerXAnchor.constraint(equalTo: layoutArea.centerXAnchor, constant: offset.x),
            centerYAnchor.constraint(equalTo: layoutArea.centerYAnchor, constant: offset.y)
        ]

        if active {
            NSLayoutConstraint.activate(constraints)
        }

        return constraints
    }
}

// MARK: Horizontal center
extension LayoutAnchorAreaType {
    /**
     Pin the view horizontal center to the given views horizontal center
     - Parameter view: the view to which this views horizontal center should relate to
     - Parameter offset: the offset from the given views center, positive will move the view to the right of the center and negative will move the view to the left of the center (in LTR)
     - Parameter layoutArea: use the views default anchors, the safe area anchors or the layout margin anchors. Defaults to `.default`.
     - Parameter active: enable the constraints. Defaults to `true`.
     - Returns: the created constraint.
     */
    @discardableResult
    public func pinCenterHorizontal(
        to view: UIView,
        offset: CGFloat = 0,
        layoutArea: UIView.LayoutArea = .default,
        active: Bool = true
    ) -> NSLayoutConstraint {
        pinCenterHorizontal(
            to: view.layoutArea(for: layoutArea),
            offset: offset,
            active: active
        )
    }

    /**
     Pin the view horizontal center to the given layout area horizontal center
     - Parameter layoutArea: the layout area to which this views horizontal center should relate to
     - Parameter offset: the offset from the given views center, positive will move the view to the right of the center and negative will move the view to the left of the center (in LTR)
     - Parameter active: enable the constraints. Defaults to `true`.
     - Returns: the created constraint.
     */
    @discardableResult
    public func pinCenterHorizontal(
        to layoutArea: LayoutAnchorAreaType,
        offset: CGFloat = 0,
        active: Bool = true
    ) -> NSLayoutConstraint {
        centerXAnchor.constraint(
            equalTo: layoutArea.centerXAnchor,
            constant: offset
        ).setActive(active)
    }
}

// MARK: Vertical center
extension LayoutAnchorAreaType {

    /**
     Pin the view vertical center to the given views vertical center
     - Parameter view: the view to which this views vertical center should relate to
     - Parameter offset: the offset from the given views center, positive will move the view down of the center and negative will move the view up from of the center
     - Parameter layoutArea: use the views default anchors, the safe area anchors or the layout margin anchors. Defaults to `.default`.
     - Parameter active: enable the constraints. Defaults to `true`.
     - Returns: the created constraint.
     */
    @discardableResult
    public func pinCenterVertical(
        to view: UIView,
        offset: CGFloat = 0,
        layoutArea: UIView.LayoutArea = .default,
        active: Bool = true
    ) -> NSLayoutConstraint {
        pinCenterVertical(
            to: view.layoutArea(for: layoutArea),
            offset: offset,
            active: active
        )
    }

    /**
     Pin the view vertical center to the given layout area vertical center
     - Parameter layoutArea: the layout area to which this views vertical center should relate to
     - Parameter offset: the offset from the given views center, positive will move the view down of the center and negative will move the view up from of the center
     - Parameter active: enable the constraints. Defaults to `true`.
     - Returns: the created constraint.
     */
    @discardableResult
    public func pinCenterVertical(
        to layoutArea: LayoutAnchorAreaType,
        offset: CGFloat = 0,
        active: Bool = true
    ) -> NSLayoutConstraint {
        centerYAnchor.constraint(
            equalTo: layoutArea.centerYAnchor,
            constant: offset
        ).setActive(active)
    }
}
