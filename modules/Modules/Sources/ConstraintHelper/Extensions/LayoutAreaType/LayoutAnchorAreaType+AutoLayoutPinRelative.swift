import UIKit

extension LayoutAnchorAreaType {
    /**
     Pin the view above the given view
     - Parameter view: the view where this view should be pinned above
     - Parameter padding: the padding between the two views. Defaults to `0`.
     - Parameter layoutArea: use the views default anchors, the safe area anchors or the layout margin anchors. Defaults to `.default`.
     - Parameter relation: constraints should be equal, lessThanOrEqual or greaterThanOrEqual. Defaults to `.equal`.
     - Parameter active: enable the constraints. Defaults to `true`.
     */
    @discardableResult
    public func pinAbove(
        view: UIView,
        padding: CGFloat = 0,
        layoutArea: UIView.LayoutArea = .default,
        relation: NSLayoutConstraint.Relation = .equal,
        active: Bool = true
    ) -> NSLayoutConstraint {
        bottomAnchor.constraint(
            to: view.layoutArea(for: layoutArea).topAnchor,
            constant: -padding,
            relation: relation
        ).setActive(active)
    }

    /**
     Pin the view below the given view
     - Parameter view: the view where this view should be pinned below
     - Parameter padding: the padding between the two views. Defaults to `0`.
     - Parameter layoutArea: use the views default anchors, the safe area anchors or the layout margin anchors. Defaults to `.default`.
     - Parameter relation: constraints should be equal, lessThanOrEqual or greaterThanOrEqual. Defaults to `.equal`.
     - Parameter active: enable the constraints. Defaults to `true`.
     */
    @discardableResult
    public func pinBelow(
        view: UIView,
        padding: CGFloat = 0,
        layoutArea: UIView.LayoutArea = .default,
        relation: NSLayoutConstraint.Relation = .equal,
        active: Bool = true
    ) -> NSLayoutConstraint {
        topAnchor.constraint(
            to: view.layoutArea(for: layoutArea).bottomAnchor,
            constant: padding,
            relation: relation
        ).setActive(active)
    }

    /**
     Pin the view after to the given view
     - Parameter view: the view where this view should be pinned after
     - Parameter padding: the padding between the two views. Defaults to `0`.
     - Parameter layoutArea: use the views default anchors, the safe area anchors or the layout margin anchors. Defaults to `.default`.
     - Parameter relation: constraints should be equal, lessThanOrEqual or greaterThanOrEqual. Defaults to `.equal`.
     - Parameter active: enable the constraints. Defaults to `true`.
     */
    @discardableResult
    public func pinAfter(
        view: UIView,
        padding: CGFloat = 0,
        layoutArea: UIView.LayoutArea = .default,
        relation: NSLayoutConstraint.Relation = .equal,
        active: Bool = true
    ) -> NSLayoutConstraint {
        leadingAnchor.constraint(
            to: view.layoutArea(for: layoutArea).trailingAnchor,
            constant: padding,
            relation: relation
        ).setActive(active)
    }

    /**
     Pin the view before the given view
     - Parameter view: the view where this view should be pinned before
     - Parameter padding: the padding between the two views. Defaults to `0`.
     - Parameter layoutArea: use the views default anchors, the safe area anchors or the layout margin anchors. Defaults to `.default`.
     - Parameter relation: constraints should be equal, lessThanOrEqual or greaterThanOrEqual. Defaults to `.equal`.
     - Parameter active: enable the constraints. Defaults to `true`.
     */
    @discardableResult
    public func pinBefore(
        view: UIView,
        padding: CGFloat = 0,
        layoutArea: UIView.LayoutArea = .default,
        relation: NSLayoutConstraint.Relation = .equal,
        active: Bool = true
    ) -> NSLayoutConstraint {
        trailingAnchor.constraint(
            to: view.layoutArea(for: layoutArea).leadingAnchor,
            constant: -padding,
            relation: relation
        ).setActive(active)
    }
}
