import UIKit

// MARK: - Entire Center
extension LayoutAreaSuperviewProvider {
    /**
     Pin the view center to its superview
     - Parameter offset: the offset from the superview center. Defaults to `.zero`.
     - Parameter layoutArea: use the views default anchors, the safe area anchors or the layout margin anchors. Defaults to `.default`.
     - Parameter active: enable the constraints. Defaults to `true`.
     - Returns: the created constraints or an empty array if the superview is not set (this is considered a programming error).
     */
    @discardableResult
    public func pinCenterToSuperview(
        offset: CGPoint = .zero,
        layoutArea: UIView.LayoutArea = .default,
        active: Bool = true
    ) -> [NSLayoutConstraint] {
        guard let superView = superview else {
            assertionFailure("Superview is not set")
            return []
        }

        return pinCenter(
            to: superView,
            offset: offset,
            layoutArea: layoutArea,
            active: active
        )
    }

    /**
     Pin the view center to the given views center
     - Parameter view: the view to which this views center should relate to.
     - Parameter offset: the offset from the given views center. Defaults to `.zero`.
     - Parameter layoutArea: use the views default anchors, the safe area anchors or the layout margin anchors. Defaults to `.default`.
     - Parameter active: enable the constraints. Defaults to `true`.
     - Returns: the created constraints.
     */
    @discardableResult
    public func pinCenter(
        to view: UIView,
        offset: CGPoint = .zero,
        layoutArea: UIView.LayoutArea = .default,
        active: Bool = true
    ) -> [NSLayoutConstraint] {
        pinCenter(
            to: view.layoutArea(for: layoutArea),
            offset: offset,
            active: active
        )
    }
}

// MARK: Horizontal center
extension LayoutAreaSuperviewProvider {
    /**
     Pin the view horizontal center to its superview horizontal center
     - Parameter offset: the offset from the given views center, positive will move the view to the right of the center and negative will move the view to the left of the center (in LTR). Defaults to `0`.
     - Parameter layoutArea: use the views default anchors, the safe area anchors or the layout margin anchors. Defaults to `.default`.
     - Parameter active: enable the constraints, defaults to `true`.
     - Returns: the created constraint or nil if the superview is not set (this is considered a programming error).
     */
    @discardableResult
    public func pinCenterHorizontalToSuperview(
        offset: CGFloat = 0,
        layoutArea: UIView.LayoutArea = .default,
        active: Bool = true
    ) -> NSLayoutConstraint? {
        guard let superView = superview else {
            assertionFailure("Superview is not set")
            return nil
        }

        return pinCenterHorizontal(
            to: superView,
            offset: offset,
            layoutArea: layoutArea,
            active: active
        )
    }
}

// MARK: Vertical center
extension LayoutAreaSuperviewProvider {
    /**
     Pin the view vertical center to its superview vertical center
     - Parameter offset: the offset from the given views center, positive will move the view down of the center and negative will move the view up from of the center. Defaults to `0`.
     - Parameter layoutArea: use the views default anchors, the safe area anchors or the layout margin anchors. Defaults to `.default`.
     - Parameter active: enable the constraints, defaults to `true`.
     - Returns: the created constraint or nil if the superview is not set (this is considered a programming error).
     */
    @discardableResult
    public func pinCenterVerticalToSuperview(
        offset: CGFloat = 0,
        layoutArea: UIView.LayoutArea = .default,
        active: Bool = true
    ) -> NSLayoutConstraint? {
        guard let superView = superview else {
            assertionFailure("Superview is not set")
            return nil
        }

        return pinCenterVertical(
            to: superView,
            offset: offset,
            layoutArea: layoutArea,
            active: active
        )
    }
}
