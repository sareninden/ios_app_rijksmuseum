import UIKit

extension LayoutAreaSuperviewProvider {
    /**
     Pin the view width related to the superview
     - Parameter multiplier: the multiplier for the width relation.
     - Parameter extraWidth: the extra width that should be added to this view. Defaults to `0`.
     - Parameter layoutArea: use the views default anchors, the safe area anchors or the layout margin anchors. Defaults to `.default`.
     - Parameter relation: constraints should be equal, lessThanOrEqual or greaterThanOrEqual. Defaults to `.equal`.
     - Parameter active: enable the constraints. Defaults to `true`.
     */
    @discardableResult
    public func pinWidthToSuperview(
        multiplier: CGFloat = 1,
        extraWidth: CGFloat = 0,
        layoutArea: UIView.LayoutArea = .default,
        relation: NSLayoutConstraint.Relation = .equal,
        active: Bool = true
    ) -> NSLayoutConstraint? {
        guard let superview = superview else {
            assertionFailure("Superview is not set")
            return nil
        }

        return pinWidth(
            to: superview.layoutArea(for: layoutArea),
            multiplier: multiplier,
            extraWidth: extraWidth,
            relation: relation,
            active: active
        )
    }

    /**
     Pin the view height related to the super view
     - Parameter multiplier: the multiplier for the height relation. Defaults to `1`.
     - Parameter extraHeight: the extra height that should be added to this view. Defaults to `0`.
     - Parameter layoutArea: use the views default anchors, the safe area anchors or the layout margin anchors. Defaults to `.default`.
     - Parameter relation: constraints should be equal, lessThanOrEqual or greaterThanOrEqual. Defaults to `.equal`.
     - Parameter active: enable the constraints. Defaults to `true`.
     */
    @discardableResult
    public func pinHeightToSuperview(
        multiplier: CGFloat = 1,
        extraHeight: CGFloat = 0,
        layoutArea: UIView.LayoutArea = .default,
        relation: NSLayoutConstraint.Relation = .equal,
        active: Bool = true
    ) -> NSLayoutConstraint? {
        guard let superview = superview else {
            assertionFailure("Superview is not set")
            return nil
        }

        return pinHeight(
            with: superview.layoutArea(for: layoutArea),
            multiplier: multiplier,
            extraHeight: extraHeight,
            relation: relation,
            active: active
        )
    }
}
