import UIKit

// MARK: - Top
extension LayoutAreaSuperviewProvider {
    /**
     Pins the top anchor of this view to the given views layout area
     - Parameter padding: the padding between the two layout anchors. Defaults to `0`.
     - Parameter layoutArea: use the views default anchors, the safe area anchors or the layout margin anchors. Defaults to `.default`.
     - Parameter relation: constraints should be equal, lessThanOrEqual or greaterThanOrEqual. Defaults to `.equal`.
     - Parameter active: enable the constraints. Defaults to `true`.
     - Returns: the created constraint or nil if the superview is not set (this is considered a programming error)
     */
    @discardableResult
    public func pinTopToSuperview(
        padding: CGFloat = 0,
        layoutArea: UIView.LayoutArea = .default,
        relation: NSLayoutConstraint.Relation = .equal,
        active: Bool = true
    ) -> NSLayoutConstraint? {
        guard let superview = superview else {
            assertionFailure("Superview is not set")
            return nil
        }

        return pinTop(
            to: superview,
            padding: padding,
            layoutArea: layoutArea,
            relation: relation,
            active: active
        )
    }
}

// MARK: - Leading
extension LayoutAreaSuperviewProvider {
    /**
     Pins the leading anchor of this view to the given views layout area
     - Parameter padding: the padding between the two layout anchors. Defaults to `0`.
     - Parameter layoutArea: use the views default anchors, the safe area anchors or the layout margin anchors. Defaults to `.default`.
     - Parameter relation: constraints should be equal, lessThanOrEqual or greaterThanOrEqual. Defaults to `.equal`.
     - Parameter active: enable the constraints. Defaults to `true`.
     - Returns: the created constraint or nil if the superview is not set (this is considered a programming error)
     */
    @discardableResult
    public func pinLeadingToSuperview(
        padding: CGFloat = 0,
        layoutArea: UIView.LayoutArea = .default,
        relation: NSLayoutConstraint.Relation = .equal,
        active: Bool = true
    ) -> NSLayoutConstraint? {
        guard let superview = superview else {
            assertionFailure("Superview is not set")
            return nil
        }

        return pinLeading(
            to: superview,
            padding: padding,
            layoutArea: layoutArea,
            relation: relation,
            active: active
        )
    }
}

// MARK: - Trailing
extension LayoutAreaSuperviewProvider {
    /**
     Pins the trailing anchor of this view to the given views layout area
     - Parameter padding: the padding between the two layout anchors. Defaults to `0`.
     - Parameter layoutArea: use the views default anchors, the safe area anchors or the layout margin anchors. Defaults to `.default`.
     - Parameter relation: constraints should be equal, lessThanOrEqual or greaterThanOrEqual. Defaults to `.equal`. Defaults to `.equal`.
     - Parameter active: enable the constraints. Defaults to `true`.
     - Returns: the created constraint or nil if the superview is not set (this is considered a programming error)
     */
    @discardableResult
    public func pinTrailingToSuperview(
        padding: CGFloat = 0,
        layoutArea: UIView.LayoutArea = .default,
        relation: NSLayoutConstraint.Relation = .equal,
        active: Bool = true
    ) -> NSLayoutConstraint? {
        guard let superview = superview else {
            assertionFailure("Superview is not set")
            return nil
        }

        return pinTrailing(
            to: superview,
            padding: padding,
            layoutArea: layoutArea,
            relation: relation,
            active: active
        )
    }
}

// MARK: - Bottom
extension LayoutAreaSuperviewProvider {
    /**
     Pins the bottom anchor of this view to the given views layout area
     - Parameter padding: the padding between the two layout anchors. Defaults to `0`.
     - Parameter layoutArea: use the views default anchors, the safe area anchors or the layout margin anchors. Defaults to `.default`.
     - Parameter relation: constraints should be equal, lessThanOrEqual or greaterThanOrEqual. Defaults to `.equal`. Defaults to `.equal`.
     - Parameter active: enable the constraints. Defaults to `true`.
     - Returns: the created constraint or nil if the superview is not set (this is considered a programming error)
     */
    @discardableResult
    public func pinBottomToSuperview(
        padding: CGFloat = 0,
        layoutArea: UIView.LayoutArea = .default,
        relation: NSLayoutConstraint.Relation = .equal,
        active: Bool = true
    ) -> NSLayoutConstraint? {
        guard let superview = superview else {
            assertionFailure("Superview is not set")
            return nil
        }

        return pinBottom(
            to: superview,
            padding: padding,
            layoutArea: layoutArea,
            relation: relation,
            active: active
        )
    }
}
