import UIKit

extension LayoutAreaSuperviewProvider {
    /**
     Pin the edges of this views to the super view.
     - Parameter padding: the padding of the edges. The bottom and right (trailing) values will be prefixed with `-`. Defaults to `.zero`.
     - Parameter layoutArea: use the views default anchors, the safe area anchors or the layout margin anchors. Defaults to `.default`.
     - Parameter excludeEdges: the edges that should not be pinned. Defaults to `.none`.
     - Parameter relation: constraints should be equal, lessThanOrEqual or greaterThanOrEqual. The value for trailing and bottom will be flipped. Defaults to `.equal`.
     - Parameter active: enable the constraints, defaults to `true`.

     - Returns: an array with the created constraints.
     */
    @discardableResult
    public func pinEdgesToSuperview(
        padding: NSDirectionalEdgeInsets = .zero,
        layoutArea: UIView.LayoutArea = .default,
        excludeEdges: NSLayoutConstraint.Edge = .none,
        relation: NSLayoutConstraint.Relation = .equal,
        active: Bool = true
    ) -> [NSLayoutConstraint] {
        let relations = NSLayoutConstraint.RelationsAround(
            top: relation,
            leading: relation,
            trailing: relation.flipped,
            bottom: relation.flipped
        )

        return pinEdgesToSuperview(
            padding: padding,
            layoutArea: layoutArea,
            excludeEdges: excludeEdges,
            relations: relations,
            active: active
        )
    }

    /**
     Pin the edges of this views to the super view.
     - Parameter padding: the padding of the edges. The bottom and right (trailing) values will be prefixed with `-`. Defaults to `.zero`.
     - Parameter layoutArea: use the views default anchors, the safe area anchors or the layout margin anchors. Defaults to `.default`.
     - Parameter excludeEdges: the edges that should not be pinned. Defaults to `.none`.
     - Parameter relations: constraints should be equal, lessThanOrEqual or greaterThanOrEqual. The value for trailing and bottom will **not** be flipped.
     The `relations` do not have a default due to signature conflicts that will arise with the functions that have `relation` as input parameter.
     - Parameter active: enable the constraints, defaults to `true`.

     - Returns: an array with the created constraints.
     */
    @discardableResult
    public func pinEdgesToSuperview(
        padding: NSDirectionalEdgeInsets = .zero,
        layoutArea: UIView.LayoutArea = .default,
        excludeEdges: NSLayoutConstraint.Edge = .none,
        relations: NSLayoutConstraint.RelationsAround,
        active: Bool = true
    ) -> [NSLayoutConstraint] {
        guard let superview = superview else {
            assertionFailure("Superview is not set")
            return []
        }

        return pinEdges(
            to: superview,
            padding: padding,
            layoutArea: layoutArea,
            excludeEdges: excludeEdges,
            relations: relations,
            active: active
        )
    }

    /**
     Pins the view vertical in the superview.
     - Parameter padding: the padding of the edges. Defaults to `0`.
     - Parameter layoutArea: use the views default anchors, the safe area anchors or the layout margin anchors. Defaults to `.default`.
     - Parameter relation: constraints should be equal, lessThanOrEqual or greaterThanOrEqual. The value for bottom will be flipped. Defaults to `.equal`.
     - Parameter active: enable the constraints, defaults to `true`.

     - Returns: an array with the created constraints.
     */
    @discardableResult
    public func pinVerticalEdgesToSuperview(
        padding: CGFloat = 0,
        layoutArea: UIView.LayoutArea = .default,
        relation: NSLayoutConstraint.Relation = .equal,
        active: Bool = true
    ) -> [NSLayoutConstraint] {
        guard let superview = superview else {
            assertionFailure("Superview is not set")
            return []
        }

        return pinVerticalEdges(
            to: superview,
            padding: padding,
            layoutArea: layoutArea,
            relation: relation,
            active: active
        )
    }

    /**
     Pins the view horizontal in the superview.
     - Parameter padding: the padding of the edges. Defaults to `0`.
     - Parameter layoutArea: use the views default anchors, the safe area anchors or the layout margin anchors. Defaults to `.default`.
     - Parameter relation: constraints should be equal, lessThanOrEqual or greaterThanOrEqual. The value for trailing will be flipped. Defaults to `.equal`.
     - Parameter active: enable the constraints, defaults to `true`.

     - Returns: an array with the created constraints.
     */
    @discardableResult
    public func pinHorizontalEdgesToSuperview(
        padding: CGFloat = 0,
        layoutArea: UIView.LayoutArea = .default,
        relation: NSLayoutConstraint.Relation = .equal,
        active: Bool = true
    ) -> [NSLayoutConstraint] {
        guard let superview = superview else {
            assertionFailure("Superview is not set")
            return []
        }

        return pinHorizontalEdges(
            to: superview,
            padding: padding,
            layoutArea: layoutArea,
            relation: relation,
            active: active
        )
    }

    /**
     Pins the view leading and top anchors to the superview.
     - Parameter padding: the padding of the edges. Defaults to `.zero`.
     - Parameter layoutArea: use the views default anchors, the safe area anchors or the layout margin anchors. Defaults to `.default`.
     - Parameter relation: constraints should be equal, lessThanOrEqual or greaterThanOrEqual. Defaults to `.equal`.
     - Parameter active: enable the constraints, defaults to `true`.

     - Returns: an array with the created constraints.
     */
    @discardableResult
    public func pinLeadingTopToSuperview(
        padding: NSDirectionalEdgeInsets = .zero,
        layoutArea: UIView.LayoutArea = .default,
        relation: NSLayoutConstraint.Relation = .equal,
        active: Bool = true
    ) -> [NSLayoutConstraint] {
        guard let superview = superview else {
            assertionFailure("Superview is not set")
            return []
        }

        return pinEdges(
            to: superview.layoutArea(for: layoutArea),
            padding: padding,
            excludeEdges: [.trailing, .bottom],
            relation: relation,
            active: active
        )
    }

    /**
     Pins the view trailing and top anchors to the superview.
     - Parameter padding: the padding of the edges. Defaults to `.zero`.
     - Parameter layoutArea: use the views default anchors, the safe area anchors or the layout margin anchors. Defaults to `.default`.
     - Parameter relation: constraints should be equal, lessThanOrEqual or greaterThanOrEqual. The value for trailing will be flipped. Defaults to `.equal`.
     - Parameter active: enable the constraints, defaults to `true`.

     - Returns: an array with the created constraints.
     */
    @discardableResult
    public func pinTrailingTopToSuperview(
        padding: NSDirectionalEdgeInsets = .zero,
        layoutArea: UIView.LayoutArea = .default,
        relation: NSLayoutConstraint.Relation = .equal,
        active: Bool = true
    ) -> [NSLayoutConstraint] {
        guard let superview = superview else {
            assertionFailure("Superview is not set")
            return []
        }

        return pinEdges(
            to: superview.layoutArea(for: layoutArea),
            padding: padding,
            excludeEdges: [.leading, .bottom],
            relation: relation,
            active: active
        )
    }
}
