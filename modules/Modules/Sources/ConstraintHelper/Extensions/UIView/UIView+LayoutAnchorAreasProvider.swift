import UIKit

extension UIView {
    /**
     Returns a layout area with the  default anchors for the view
     */
    var defaultLayoutArea: LayoutAnchorArea {
        LayoutAnchorArea(view: self)
    }

    /**
     Returns a layout area with the safe area anchors
     */
    var safeLayoutArea: LayoutAnchorArea {
        LayoutAnchorArea(layoutGuide: safeAreaLayoutGuide)
    }

    /**
     Returns a layout area with the layout margins anchors
     */
    var layoutMarginLayoutArea: LayoutAnchorArea {
        LayoutAnchorArea(layoutGuide: layoutMarginsGuide)
    }

    /**
     Returns a layout area with the readable content guide anchors
     */
    var readableContentLayoutArea: LayoutAnchorArea {
        LayoutAnchorArea(layoutGuide: readableContentGuide)
    }

    /**
     Returns the anchors and dimensions related to the given layout area
     - Parameter layoutArea: which layout area anchors should be returned
     */
    func layoutArea(for layoutArea: LayoutArea) -> LayoutAnchorArea {
        LayoutAnchorArea(layoutAreaType: contentGuide(for: layoutArea))
    }

    func contentGuide(for layoutAreaType: LayoutArea) -> LayoutAnchorAreaType {
        switch layoutAreaType {
        case .default:
            return self
        case .safeArea:
            return safeAreaLayoutGuide
        case .layoutMargins:
            return layoutMarginsGuide
        case .readableContent:
            return readableContentGuide
        }
    }
}
