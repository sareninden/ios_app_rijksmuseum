import UIKit

extension UIView {
    /**
     Sets `translatesAutoresizingMaskIntoConstraints` to false
     */
    @discardableResult
    public func activateAutoLayout() -> Self {
        translatesAutoresizingMaskIntoConstraints = false

        return self
    }
}
