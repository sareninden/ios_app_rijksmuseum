import UIKit

public enum Position: String, Hashable {
    case `default`, vertical, horizontal, top, left, bottom, right
}

public protocol Sideble: ExpressibleByDictionaryLiteral, Equatable {
    var top: CGFloat { get set }
    var left: CGFloat { get set }
    var bottom: CGFloat { get set }
    var right: CGFloat { get set }
    init(top: CGFloat, left: CGFloat, bottom: CGFloat, right: CGFloat)
}

public extension Sideble {
    var width: CGFloat { left + right }
    var height: CGFloat { top + bottom }

    init(horizontal: CGFloat = 0, vertical: CGFloat = 0) {
        self.init(top: vertical, left: horizontal, bottom: vertical, right: horizontal)
    }

    init(value: CGFloat) {
        self.init(top: value, left: value, bottom: value, right: value)
    }
}

extension Sideble {
    public init(dictionaryLiteral elements: (Position, CGFloat)...) {
        var `default`: CGFloat = 0
        var vertical, horizontal, top, left, bottom, right: CGFloat?

        elements.forEach { (position, value) in
            switch position {
            case .default: `default` = value
            case .vertical: vertical = value
            case .horizontal: horizontal = value
            case .top: top = value
            case .left: left = value
            case .bottom: bottom = value
            case .right: right = value
            }
        }

        self = Self(
            top: top ?? vertical ?? `default`,
            left: left ?? horizontal ?? `default`,
            bottom: bottom ?? vertical ?? `default`,
            right: right ?? horizontal ?? `default`
        )
    }
}
