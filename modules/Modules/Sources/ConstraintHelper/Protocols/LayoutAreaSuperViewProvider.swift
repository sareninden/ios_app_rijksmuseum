import UIKit

public protocol LayoutAreaSuperviewProvider: LayoutAnchorAreaType, UIParentViewProvider {}

extension UIView: LayoutAreaSuperviewProvider {}
extension UILayoutGuide: LayoutAreaSuperviewProvider {}
