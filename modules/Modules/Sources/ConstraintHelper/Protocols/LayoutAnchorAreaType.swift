import UIKit

/**
 A protocol that encapsulates some layout anchors allowing to write generic functions for both `UIView`, `UILayoutGuide` or custom layout areas
 */
public protocol LayoutAnchorAreaType {
    var topAnchor: NSLayoutYAxisAnchor { get }
    var leadingAnchor: NSLayoutXAxisAnchor { get }
    var bottomAnchor: NSLayoutYAxisAnchor { get }
    var trailingAnchor: NSLayoutXAxisAnchor { get }
    var widthAnchor: NSLayoutDimension { get }
    var heightAnchor: NSLayoutDimension { get }
    var centerXAnchor: NSLayoutXAxisAnchor { get }
    var centerYAnchor: NSLayoutYAxisAnchor { get }
}

extension UIView: LayoutAnchorAreaType {}
extension UILayoutGuide: LayoutAnchorAreaType {}
