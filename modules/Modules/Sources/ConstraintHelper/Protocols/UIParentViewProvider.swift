import UIKit

public protocol UIParentViewProvider {
    var superview: UIView? { get }
}

extension UIView: UIParentViewProvider {}

extension UILayoutGuide: UIParentViewProvider {
    public var superview: UIView? { owningView }
}
