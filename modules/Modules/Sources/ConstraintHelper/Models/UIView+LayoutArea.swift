import UIKit

extension UIView {
    /**
     Identifier enum for the different kind of areas you can use to pin a view to
     */
    public enum LayoutArea {
        case `default`, safeArea, layoutMargins, readableContent
    }
}
