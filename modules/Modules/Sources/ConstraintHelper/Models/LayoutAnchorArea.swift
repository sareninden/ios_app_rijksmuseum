import UIKit

/**
 Helper class that contains many of a view its anchors and dimensions.
 This is used i.e. to abstract the usage of which layout area of the view is used (default, layoutMargins, safeArea)
 */
public struct LayoutAnchorArea: LayoutAnchorAreaType, Equatable {
    public let topAnchor: NSLayoutYAxisAnchor
    public let leadingAnchor: NSLayoutXAxisAnchor
    public let bottomAnchor: NSLayoutYAxisAnchor
    public let trailingAnchor: NSLayoutXAxisAnchor
    public let widthAnchor: NSLayoutDimension
    public let heightAnchor: NSLayoutDimension
    public let centerXAnchor: NSLayoutXAxisAnchor
    public let centerYAnchor: NSLayoutYAxisAnchor

    public init(
        topAnchor: NSLayoutYAxisAnchor,
        leadingAnchor: NSLayoutXAxisAnchor,
        bottomAnchor: NSLayoutYAxisAnchor,
        trailingAnchor: NSLayoutXAxisAnchor,
        widthAnchor: NSLayoutDimension,
        heightAnchor: NSLayoutDimension,
        centerXAnchor: NSLayoutXAxisAnchor,
        centerYAnchor: NSLayoutYAxisAnchor
    ) {
        self.topAnchor = topAnchor
        self.leadingAnchor = leadingAnchor
        self.bottomAnchor = bottomAnchor
        self.trailingAnchor = trailingAnchor
        self.widthAnchor = widthAnchor
        self.heightAnchor = heightAnchor
        self.centerXAnchor = centerXAnchor
        self.centerYAnchor = centerYAnchor
    }

    public init(layoutAreaType: LayoutAnchorAreaType) {
        self.init(
            topAnchor: layoutAreaType.topAnchor,
            leadingAnchor: layoutAreaType.leadingAnchor,
            bottomAnchor: layoutAreaType.bottomAnchor,
            trailingAnchor: layoutAreaType.trailingAnchor,
            widthAnchor: layoutAreaType.widthAnchor,
            heightAnchor: layoutAreaType.heightAnchor,
            centerXAnchor: layoutAreaType.centerXAnchor,
            centerYAnchor: layoutAreaType.centerYAnchor
        )
    }

    public init(view: UIView) {
        self.init(
            topAnchor: view.topAnchor,
            leadingAnchor: view.leadingAnchor,
            bottomAnchor: view.bottomAnchor,
            trailingAnchor: view.trailingAnchor,
            widthAnchor: view.widthAnchor,
            heightAnchor: view.heightAnchor,
            centerXAnchor: view.centerXAnchor,
            centerYAnchor: view.centerYAnchor
        )
    }

    public init(layoutGuide: UILayoutGuide) {
        self.init(
            topAnchor: layoutGuide.topAnchor,
            leadingAnchor: layoutGuide.leadingAnchor,
            bottomAnchor: layoutGuide.bottomAnchor,
            trailingAnchor: layoutGuide.trailingAnchor,
            widthAnchor: layoutGuide.widthAnchor,
            heightAnchor: layoutGuide.heightAnchor,
            centerXAnchor: layoutGuide.centerXAnchor,
            centerYAnchor: layoutGuide.centerYAnchor
        )
    }
}
