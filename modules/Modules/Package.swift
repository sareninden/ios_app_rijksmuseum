// swift-tools-version:5.3
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

extension String {
    var firstCapitalized: String { prefix(1).capitalized + dropFirst() }
}

enum LibraryProducts: String, CaseIterable {
    // App Generic
    case appearance, constraintHelper, rijksMuseumApi

    var name: String { rawValue.firstCapitalized }
    var dependencyName: String { name }
    var targetName: String { name }
    var targetTestName: String { name + "Tests" }
    
    var product: PackageDescription.Product { .library(name: name, targets: [name]) }
    var dependency: Target.Dependency { .byName(name: dependencyName) }
    
    static var allProducts: [PackageDescription.Product] { LibraryProducts.allCases.map(\.product) }
    
    static func dependencies(for products: [LibraryProducts]) -> [Target.Dependency] {
        products.map(\.dependency)
    }
}

let package = Package(
    name: "Modules",
    platforms: [.iOS(.v14)],
    products: LibraryProducts.allProducts,
    dependencies: [],
    targets: [
        .target(name: LibraryProducts.appearance.targetName),
        .target(name: LibraryProducts.constraintHelper.targetName),
        .target(name: LibraryProducts.rijksMuseumApi.targetName),
        .testTarget(
            name: LibraryProducts.rijksMuseumApi.targetTestName,
            dependencies: LibraryProducts.dependencies(for: [.rijksMuseumApi])
        )
    ]
)
